#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)
#include <stdio.h>
//int main()
//{
//	int aa[2][5] = { 10,9,8,7,6,5,4,3,2,1 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}
//int main()
//{
//	int a[5] = { 5, 4, 3, 2, 1 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d,%d", *(a + 1), *(ptr - 1));
//	return 0;
//}




//int main()
//{
//	////数组名
//
//	//int arr[10] = { 0 };
//	//printf("%d\n", sizeof(arr));
//	//printf("%p\n", arr);
//	//printf("%p\n", &arr[0]);
//	//printf("%p\n", &arr);
// //   //数组名绝大多数情况下是数组首元素地址
//	////但有2个例外
//	////1. sizeof（数组名） - sizeof内部单独放一个数组名的时候，计算得到的是数组的总大小
//	////2. &arr - 这里的数组名表示整个数组，取出的是整个数组的地址，从地址值的角度来讲和数组首元素的地址是一样的，但是意义不一样
//	//
//	////这里的就是两者的区别
//	//printf("%p\n", arr+1);//int* - 4
//	//printf("%p\n", &arr[0]+1);//int* - 4
//	//printf("%p\n", &arr+1);//int (*)[10] - 40
//	//
//	
//	
//	//数组指针的使用
//
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	////下标的形式访问数组
//	//for (i = 0; i < sz; i++)
//	//{
//	//	printf("%d ", arr[i]);
//	//}
//	////使用指针来访问
//	//int* parr = arr;
//	//for (i = 0; i < sz; i++)
//	//{
//	//	printf("%d ", *(parr + i));
//	//}
//	////不建议
//	//int(*pparr)[10] = &arr;
//	//for (i = 0; i < sz; i++)
//	//{
//	//	
//	//	printf("%d ", *((*pparr) + i));
//	//	//pparr  ---  &arr
//	//	//*pparr   ---  *&arr
//	//	//*pparr  ---  arr
//
//	//	//*((*pparr) + i) = (*pparr)[i];
//	//}
//	
//
//
//	return 0;
//}

////一维数组传参，形参是数组
//void print1(int arr[10], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
////一维数组传参，形参是指针
//void print(int *arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		//printf("%d ", arr[i]);  -   也可行
//		printf("%d ", *(arr+i));
//	}
//	printf("\n");
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(arr, sz);
//
//	return 0;
//}

////二维数组传参，形参是数组
//void print1(int arr[3][5], int r, int c)
//{
//	int i = 0;
//	for (i = 0; i < r; i++)
//	{
//		int j = 0;
//		for (j = 0; j < c; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	printf("\n");
//}
//
////二维数组传参，形参是指针
//void print(int(*arr)[5], int r, int c)
//{
//	int i = 0;
//	for (i = 0; i < r; i++)
//	{
//		int j = 0;
//		for (j = 0; j < c; j++)
//		{
//			//printf("%d ", arr[i][j]);
//			printf("%d ", *((*arr + i) + j));
//		}
//		printf("\n");
//	}
//	printf("\n");
//}
//int main()
//{
//	int arr[3][5] = { 1,2,3,4,5,2,3,4,5,6,3,4,5,6,7 };
//	//二维数组的数组名，也表示首元素的地址
//	//二维数组的首元素是第一行
//	//二维数组其实是一维数组的数组
//	//首元素的地址就是第一行的地址，是一个一维数组的地址
//
//	print(arr, 3, 5);
//
//	return 0;
//}

//int main()
//{
//
//	int arr[5];
//	int* parr1[10];
//	int(*parr2)[10];
//	int(*parr3[10])[5]; // 存放数组指针的一个数组
//
//	return 0;
//}


//数组传参

//void test(int arr[])  //ok?
//{
//	//数组传参用数组接收，可以
//	//此时 所传的不是一个数组，而是数组首元素的地址，所以可以不规定元素个数
//}
//void test(int arr[10])  //ok?
//{
//	//也可以规定元素个数
//}
//void test(int *arr)  //ok?
//{
//	//数组名是首元素地址，用指针也可以
//}
//void test2(int *arr[20])  //ok?
//{
//	//指针数组传过来，用指针数组接收一点毛病没有
//}
//void test2(int **arr)  //ok?
//{
//	//arr2是指针数组的首元素地址，每个元素都是一个地址，所以用**arr接收是可以的
//	//先解引用符是找到arr2指针数组中的第一个元素的地址，
//	//第二个解引用符是找到第一个元素中存放的地址的值
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	int* arr2[20] = { 0 };
//	test(arr);
//	test2(arr2);
//
//	return 0;
//}


//void test(int arr[3][5])  //ok?
//{
//	//一模一样接收当然可以
//}
//void test(int arr[][])  //ok?
//{
//	//二维数组必须要规定行数，所以不能全都省略
//}
//void test(int arr[][5]) //ok?
//{
//	//可以
//}
//void test(int* arr)  //ok?
//{
//	//二维数组只用一个指针接收当然不行
//	//一个解引用操作符，只能找到一个地址，他不能找到二维数组中列的元素
//  //因为传过来的是第一行的地址，所以+1会跳到下一行也就是第二行，根本不会去找列中的元素
//  //但有的uu可能会说，他这样写出来的代码就是能够运行，也能找到列中的元素
//  //这是因为编译器在翻译过程中，已经自动处理了你的代码，所以才会显得出代码正确可以运行的假象
//}
//void test(int *arr[5])  //ok?
//{
//	//这个也不行，因为arr传过来的是第一行的地址，只是一个地址，而*arr[5]
//	//arr会先和[]结合为一个数组。这个数组有五个整型指针的元素，
//	//一个地址不需要五个地址存放，所以这个也是不行的
//}
//void test(int (*arr)[5])  //ok?
//{
//	//这个就规避掉了上面的问题，arr先和*结合找到第一行首元素的地址，
//	//再[]结合找到每一行中对应列上的地址，
//	//可以这样写 (*(arr + i))[j];
//}
//void test(int **arr)  //ok?
//{
//	//这个是不行的
//	//二级指针是用来接收一级指针地址的，是需要地址中再存放地址的，
//	//而二维数组的数组名只是传的首元素的地址（一个地址）所以不行哦
//}
//
//int main()
//{
//	int arr[3][5] = { 0 };
//	test(arr);
//  
//  //总结
//  //二维数组传参
//  //参数可以是指针，也可以是数组
//  //如果是数组，行可以省略，但是列不能省略
// 
//  //如果是指针，传过去的是第一行的地址，形参就应该是数组指针
//
// 
//	return 0;
//}



//指针传参



//void print(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d\n", * (p + i));
//	}
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	print(p,sz);
//	return 0;
//}
//// 思考：
//// 当一个函数的参数部分为一级指针的时候，函数能接收什么参数？
//// 整型变量的地址，整形指针，整型数组的数组名


//
//void test(int** ptr)
//{
//	printf("%d\n", **ptr);
//}
//int main()
//{
//	int n = 10;
//	int* p = &n;
//	int** pp = &p;
//	test(pp);
//	test(&p);
//
//	return 0;
//}
//// 思考：
//// 当函数的参数为二级指针的时候，可以接收什么参数？
//// 二级指针变量，一级指针变量的地址，指针数组的数组名



//函数指针


//int Add(int x, int y)
//{
//	return x + y;
//}
//
//&函数名得到就是函数的地址
//这里可能有同学回想，这与数组中的&数组名和数组名一样吗？是两种不同的结果吗
//答案是&函数名和函数名是一样的，与数组不同
//
//int main()
//{
//	//printf("%p\n", &Add);
//	//printf("%p\n", Add);
// 
//	//pf就是函数指针
//	int (* pf)(int, int) = Add;//函数的地址要存起来，就得放在【函数指针变量】中
//	
//	//这里就有uu在想，我们该如何利用函数指针调用函数呢，其实就是这个样 int ret = (*pf)(3, 5);
//	//而我们以前调用函数都是这样的 int ret = Add(3, 5); 并且在 int (* pf)(int, int) = Add; 中，去掉名就是类型，所以是将Add存放在pf中了
//	//所以 int ret = pf(3, 5); 这样也是可以的，所以说，*就是一个摆设，是一个可有可无的存在，
//  //如果你想的话，也可以这样写 int ret = (**************pf)(3, 5); 都无妨
//
//	printf("%d\n", ret);
//
//	return 0;
//}


//趣味代码 -- 有难度

////代码1
//(*(void (*)())0)();
////代码2
//void (*signal(int, void(*)(int)))(int);



//函数指针数组

//
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//int main()
//{
//	int (*pf[4])(int, int) = { Add,Sub,Mul,Div };
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		int ret = pf[i](8, 4);
//		printf("%d\n", ret);
//	}
//	return 0;
//}

//
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//
//	do
//	{
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//
//		switch (input)
//		{
//		case 0:
//			printf("退出计算器\n");
//			break;
//		case 1:
//			printf("请输入两个操作数:>");
//			scanf("%d %d", &x, &y);
//			ret = Add(x, y);
//			printf("%d\n", ret);
//			break;
//		case 2:
//			printf("请输入两个操作数:>");
//			scanf("%d %d", &x, &y);
//			ret = Sub(x, y);
//			printf("%d\n", ret);
//			break;
//		case 3:
//			printf("请输入两个操作数:>");
//			scanf("%d %d", &x, &y);
//			ret = Mul(x, y);
//			printf("%d\n", ret);
//			break;
//		case 4:
//			printf("请输入两个操作数:>");
//			scanf("%d %d", &x, &y);
//			ret = Div(x, y);
//			printf("%d\n", ret);
//			break;
//		default:
//			printf("输入错误，请重新选择:>");
//			break;
//		}
//	} while (input);
//	return 0;
//}

////写一个计算器
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//void menu()
//{
//	printf("*******************************\n");
//	printf("****** 1. Add    2. Sub  ******\n");
//	printf("****** 3. Mul    4. Div  ******\n");
//	printf("****** 0. exit           ******\n");
//	printf("*******************************\n");
//}
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	int (*pfarr[])(int, int) = { 0,Add,Sub,Mul,Div };
//	do
//	{
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		if (input == 0)
//		{
//			printf("退出计算器\n");
//			break;
//		}
//		else if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个操作数:>");
//			scanf("%d %d", &x, &y);
//			ret = pfarr[input](x, y);
//			printf("%d\n", ret);
//		}
//		else
//		{
//			printf("选择错误\n");
//		}
//		
//	} while (input);
//	return 0;
//}


int int_cmp(const void* p1, const void* p2)
{
	return (*(int*)p1 - *(int*)p2);
}
void _swap(void* p1, void* p2, int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		char tmp = *((char*)p1 + i);
		*((char*)p1 + i) = *((char*)p2 + i);
		*((char*)p2 + i) = tmp;
	}
}
void bubble(void* base, int count, int size, int(*cmp)(void*, void*))
{
	int i = 0;
	int j = 0;

	for (i = 0; i < count - 1; i++)
	{
		for (j = 0; j < count - i - 1; j++)
		{
			if (cmp((char*)base + j * size, (char*)base + (j + 1) * size) > 0)
			{
				_swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
			}
		}
	}
}
int main()
{
	int arr[] = { 1, 3, 5, 7, 9, 2, 4, 6, 8, 0 };
	char *arr[] = {"aaaa","dddd","cccc","bbbb"};
	int i = 0;
	bubble(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_cmp);
	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	return 0;
}



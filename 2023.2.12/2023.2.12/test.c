#define _CRT_SECURE_NO_WARNINGS 1
//
// F5 开始调试  -  执行逻辑上的下一个断点（不是物理上的下一个断点）
// Ctrl + F5 开始执行（调试） 
// F9 设断点  -  可以设置条件断点（亦可在循环中创建一个条件）
//F10 逐语句
// F11 逐过程 - 可以进入函数
//F5和F9 配合使用的


//#include<stdio.h>
//void test()
//{
//	printf("haha");
//}
//int main()
//{
//	int i = 0;
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	test();
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", i);
//	}
//	return 0;
//}

//void test2()
//{
//	printf("hehe/n");
//}
//void test1()
//{
//	test2();
//}
//void test()
//{
//	test1();
//}
//int main()
//{
//	test();
//	return 0;
//}
//

//void test(int arr[])  // 在调试中 函数传参的数组为首元素地址 - 在数组后加（数字）就能看到数组后的内容
//{
//
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
//	test(arr);
//	return 0;
//}


//int main()
//{
//	int n = 0;
//	scanf("%d ", &n);
//	int sum = 0;
//	for (int i = 1; i <= n; i++)
//	{
//		int ret = 1;
//		ret = ret * i;
//		sum += ret;
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//int main()   //   越界访问+死循环   -   ( 在 vc6.0 中没空隙 ； 在 gcc 中有一个整型空隙 ； 在 vc 中有两个整型空隙）
//{
//	int i = 0;
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (i = 0; i <= 12; i++)
//	{
//		arr[i] = 0;
//		printf("hehe\n");
//	}
//	return 0;
//}






//作业
// 
//      奇数偶数排序
//#include<stdio.h>
//void OddEvenAdjust(int arr[],const int sz)
//{
//	//本质为冒泡排序
//	for (int j = 0; j < sz - 1; j++)
//	{
//		for (int i = 0; i < sz - j - 1; i++)
//		{
//			//前者为偶数，后者为奇数
//			if (arr[i] % 2 == 0 && arr[i + 1] % 2 != 0)
//			{
//				int tmp = 0;
//				tmp = arr[i];
//				arr[i] = arr[i + 1];
//				arr[i + 1] = tmp;
//			}
//		}
//	}
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//计算数组的大小
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	OddEvenAdjust(arr,sz);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}



//     strcpy模拟实现
//#include<stdio.h>
//#include<assert.h>
////函数的返回值是目标数组的起始地址
//char* my_strcpy(char* destination, const char* source)
//{
//	//断言 - 防止空指针的传入
//	assert(destination != NULL);
//	assert(source != NULL);
//	//因为在拷贝过程中 目标数组的指针已经变动，不再是起始地址，所以用ret来储存destination的起始地址
//	char* ret = destination;
//	//将source数组的每一个元素拷贝到destination数组中  包括\0
//	while (*destination++ = *source++)
//	{
//		;
//	}
//
//	return ret;
//}
//int main()
//{
//	char arr1[] = "qqqqqqqqqqqq";
//	char arr2[] = "wwwwwwwwwwww";
//	printf("%s\n", my_strcpy(arr2,arr1));
//	return 0;
//}



//  strlen 函数的模拟实现
#include<stdio.h>
#include<assert.h>

int my_strlen(char* str)
{
	assert(str != NULL);
	int num = 0;
	while (*str++ != '\0')
	{
		num++;
		
	}

	return num;
}
int main()
{
	char arr[] = "123456";
	printf("%d\n",my_strlen(arr));

	return 0;
}
#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

//#include <stdio.h>
//int main()
//{
//	int arr[10][10] = { 0 };
//	int n = 0;
//	int m = 0;
//	scanf("%d %d", &n, &m);
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			int tmp = arr[i][j];
//			arr[i][j] = arr[j][i];
//			arr[j][i] = tmp;
//		}
//	}
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//BC139
//#include <stdio.h>
//int main()
//{
//	int n = 0;
//	int m = 0;
//	int arr[10][10] = { 0 };
//	scanf("%d %d", &n, &m);
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	int k = 0;
//	scanf("%d", &k);
//	getchar();
//	while (k--)
//	{
//		char t = 0;
//		int a = 0;
//		int b = 0;
//		scanf("%c %d %d", &t, &a, &b);
//		if (t == 'r')
//		{
//			for (i = 0; i < m; i++)
//			{
//				int tmp = arr[a-1][i];
//				arr[a-1][i] = arr[b-1][i];
//				arr[b-1][i] = tmp;
//			}
//		}
//		else if (t == 'c')
//		{
//			for (i = 0; i < n; i++)
//			{
//				int tmp = arr[i][a-1];
//				arr[i][a-1] = arr[i][b-1];
//				arr[i][b-1] = tmp;
//			}
//		}
//	}
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//BC140
//#include <stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[10][10] = { 1 };
//	int i = 0;
//	int j = 0;
//	for (i = 1; i < n; i++)
//	{
//		arr[i][0] = 1;
//		for (j = 1; j <= i; j++)
//		{
//			arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
//		}
//	}
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j <= i; j++)
//		{
//			printf("%5d", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[30] = { 1 };
//	printf("%5d\n", arr[0]);
//	int i = 0;
//	int j = 0;
//	for (i = 1; i < n; i++)
//	{                            // 1
//		for (j = i; j > 0; j--)  // 1 1
//		{                        // 1 2 1
//			arr[j] += arr[j - 1];// 1 3 3 1
//		}
//		for (j = 0; j <= i; j++)
//		{
//			printf("%5d", arr[j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}


//模拟atoi函数
//#include <stdio.h>
//#include <ctype.h>
//long long my_atoi(char* str)
//{
//	long long n = 0;
//	int flag = 0;
//	while (isspace(*str)) // 去掉空格
//	{
//		str++;
//	}
//	if (*str == '-') // 判断正负
//	{
//		flag = 1;
//		str++;
//	}
//	else if (*str == '+')
//	{
//		flag = 0;
//		str++;
//	}
//
//	while (isdigit(*str))  //  转换数字
//	{
//		n = n * 10 + (*str - '0');
//		str++;
//	}
//	return n;
//}
//int main()
//{
//	char arr[20] = { 0 };
//	scanf("%s", &arr);
//	long long ret = my_atoi(arr);
//	printf("%lld\n", ret);
//	return 0;
//}

#include <stdio.h>
//int main()
//{
//	printf("printf(\" hello world\\n\");\n");
//	printf("cout << \"Hello world!\" << endl;");
//	return 0;
//}

//去重并排序——法一（正常做法）
//int main()
//{
//	int n = 0;
//	int arr[6] = { 0 };
//	scanf("%d", &n);
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	for (j = 0; j < n - 1; j++)
//	{
//		for (i = 0; i < n - j - 1; i++)
//		{
//			if (arr[i] > arr[i + 1])
//			{
//				int tmp = arr[i];
//				arr[i] = arr[i + 1];
//				arr[i + 1] = tmp;
//			}
//		}
//	}
//	for (j = 0, i = 0; i < n - 1; i++)
//	{
//		if (arr[i] == arr[i + 1])
//		{
//			for (j = i; j < n - 1; j++)
//			{
//				arr[j] = arr[j + 1];
//			}
//			n--;
//			i--;
//		}
//	}
//	for (i = 0; i < n; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	
//	return 0;
//}
////法二
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[1001] = { 0 };
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		int m = 0;
//		scanf("%d", &m);
//		arr[m] = m;
//	}
//	for (i = 0; i < 1001; i++)
//	{
//		if (arr[i] != 0)
//		{
//			printf("%d ", arr[i]);
//		}
//	}
//	return 0;
//}

//
//#include <stdio.h>
//#include <stdlib.h>
//
//int main(int argc, char* argv[])
//{
//    // 请在此输入您的代码
//    int n = 0;
//    int max = 0;
//    int min = 100;
//    float av = 0.0f;
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        int tmp = 0;
//        scanf("%d", &tmp);
//        if (tmp > max)
//        {
//            max = tmp;
//        }
//        if (tmp < min)
//        {
//            min = tmp;
//        }
//        av += tmp;
//    }
//    printf("%d\n%d\n%.2f\n", max, min, (av + 0.005)/n);
//
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int n = 0;
//	scanf("%d %d %d", &a, &b, &n);
//	int count = 0;
//	int day = 0;
//
//	while (n>0)
//	{
//		day %= 7;
//		if (day == 5 || day == 6)
//		{
//			n = n - b;
//		}
//		else
//		{
//			n = n - a;
//		}
//		count++;
//		day++;
//	}
//	printf("%d", count);
//	return 0;
//}

#include <stdio.h>
int main(int argc, char* argv[])
{
	char arr1[10][10] = { {"geng"},{"xin"},{"ren"},{"gui"},{"jia"},{"yi"},{"bing"},{"ding"},{"wu"},{"ji"} };
	char arr2[12][10] = { {"zi"},{"chou"},{"yin"},{"mao"},{"chen"},{"si"},{"wu"},{"wei"},{"shen"},{"you"},{"xu"},{"hai"} };
	int n = 0;
	scanf("%d", &n);
	int t = 0;
	int d = 0;
	int y = 0;
	if (n == 2020)
	{
		printf("gengzi");
	}
	else if (n > 2020)
	{
		y = n - 2020;
		t = y % 10;
		d = y % 12;
		printf("%s%s", arr1[t], arr2[d]);
	}
	else if (n < 2020)
	{
		y = 2020 - n;
		t = y % 10;
		d = y % 12;
		printf("%s%s", arr1[t], arr2[d]);
	}
	return 0;
}
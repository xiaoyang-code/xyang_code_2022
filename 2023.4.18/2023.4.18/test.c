#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int Sub(int x, int y)
//{
//	return x - y;
//}
//
//int Mul(int x, int y)
//{
//	return x * y;
//}

//void Calc(int(*pf)(int, int))
//{
//	int x = 0, y = 0;
//	printf("请输入操作数:>");
//	scanf("%d %d", &x, &y);
//	int ret = pf(x, y);
//	printf("%d\n", ret);
//}
//
//int main()
//{
//	int a = 0;
//	do
//	{
//		printf("请选择:>");
//		scanf("%d", &a);
//		switch (a)
//		{
//		case 0 :
//			printf("退出计算器\n");
//			break;
//		case 1:
//			Calc(Add);
//			break;
//		case 2 :
//			Calc(Sub);
//			break;
//		case 3 :
//			Calc(Mul);
//			break;
//		default:
//			printf("选择错误\n");
//			break;
//		}
//	} while (a);
//	return 0;
//}


//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//	int(*pfArr[])(int, int) = { NULL,Add,Sub,Mul };
//	int ret = pfArr[a](x, y);
//	printf("%d\n", ret);
//	return 0;
//}

//函数指针
//int (*pf)(int, int);

//函数指针数组
//int (*pfArr[])(int, int);

//指向函数指针数组的指针
//int (*(*ppfArr)[])(int, int);

//int cmp(void* e1, void* e2)
//{
//	return *((int*)e1) - *((int*)e2);
//}
//
//void swap(char* buf1, char* buf2, size_t width)
//{
//	while (width--)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//	
//}
//
//void my_bsort(void* base, size_t sz, size_t width, int(*cmp)(void*, void*))
//{
//	size_t i = 0;
//	size_t j = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		for (j = 0; j < sz - i -1; j++)
//		{
//			if (cmp((((char*)base) + j * width), (((char*)base) + (j + 1) * width)) > 0)
//			{
//				swap((((char*)base) + j * width), (((char*)base) + (j + 1) * width), width);
//			}
//		}
//	}
//}
//
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	my_bsort(arr, sz, sizeof(arr[0]), cmp);
//	for (size_t i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//
//int cmp(void* e1, void* e2)
//{
//	return *((int*)e1) - *((int*)e2);
//}
//
//void swap(char* buf1, char* buf2, size_t width)
//{
//	while (width--)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//
//void Bsort(void* base, size_t sz, size_t width, int(*cmp)(void*, void*))
//{
//	for (size_t i = 0; i < sz - 1; i++)
//	{
//		for (size_t j = 0; j < sz - i - 1; j++)
//		{
//			if (cmp((((char*)base) + j * width), (((char*)base) + (j + 1) * width)) > 0)
//			{
//				swap((((char*)base) + j * width), (((char*)base) + (j + 1) * width), width);
//			}
//		}
//	}
//}
//
//int main()
//{
//	int arr[] = { 0,9,8,7,6,5,4,3,2,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Bsort(arr, sz, sizeof(arr[0]), cmp);
//	for (size_t i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//



//
//int main()
//{
//	int arr[] = { 0 };
//	int(*pf)[] = &arr;
//	(*pf)[0] = 1;
//	printf("%d ", arr[0]);
//	return 0;
//}



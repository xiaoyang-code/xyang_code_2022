#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr[] = "abcd";
//	int len = strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr1[] = "aaaaa";
//	char arr2[] = "bb bb";
//	strcpy(arr1, arr2);
//	printf("arr1 = %s\narr2 = %s", arr1, arr2);
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//	strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr1[] = "abcc";
//	char arr2[] = "abcd";
//	int len = strcmp(arr1, arr2);
//	printf("%d\n", len);
//	if (len > 0)
//	{
//		printf(">\n");
//	}
//	else if (len == 0)
//	{
//		printf("=\n");
//	}
//	else
//	{
//		printf("<\n");
//	}
//	return 0;
//}

//
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr1[] = "abcde";
//	char arr2[] = "ooooo";
//	strncpy(arr1, arr2, 3);
//	printf("%s\n", arr1);
//	return 0;
//}


//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//	strncat(arr1, arr2,3);
//	printf("%s\n", arr1);
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr1[] = "abcc";
//	char arr2[] = "abcd";
//	int len = strncmp(arr1, arr2,3);
//	printf("%d\n", len);
//	if (len > 0)
//	{
//		printf(">\n");
//	}
//	else if (len == 0)
//	{
//		printf("=\n");
//	}
//	else
//	{
//		printf("<\n");
//	}
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr[] = "this is a simple string";
//	char* ps;
//	ps = strstr(arr, "simple");
//	printf("%s", ps);
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr[20] = "329.321.33.2";
//	char* p = "."; // 存放所有的分隔符
//	char* ps;
//	for (ps = strtok(arr, p); ps != NULL; ps = strtok(NULL, p))
//	{
//		printf("%s\n", ps);
//	}
//	return 0;
//}


//#include <stdio.h>
//#include <string.h>
//#include <errno.h>
//int main()
//{
//	//打开文件
//	//打开文件的时候，如果文件的打开方式是"r"
//	//文件存在则打开成功，文件不存在打开失败
//	//打开文件失败的话，会返回NULL
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//
//		perror("打开文件失败");
//		//perror  == strerror + printf;
//		return 1;
//	}
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//#include <stdio.h>
//#include <ctype.h>
//int main()
//{
//	char arr[20] = "This Is A Boy";
//	int i = 0;
//	for (i = 0; i < 20; i++)
//	{
//		if (toupper(arr[i]) > 0)
//		{
//			arr[i] = tolower(arr[i]);
//		}
//	}
//	printf("%s\n", arr);
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[20] = { 0 };
//	memcpy(arr2, arr, 20);
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	int arr[20] = { 1,2,3,4,5,6,7,8,9,10 };
//	memmove(arr, arr + 3, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main()
//{
//	char arr1[] = "qqqwwwweeee";
//	char arr2[] = "qqqwwweeeee";
//	int len = 0;
//	len = memcmp(arr1, arr2, sizeof(arr1));
//	printf("%d\n", len);
//	return 0;
//}

//#include <stdio.h>
//#include <assert.h>
//int my_strlen1(char* arr)
//{
//	assert(arr != NULL);
//	int num = 0;
//	while (*arr)
//	{
//		num++;
//		arr++;
//	}
//	return num;
//}
//int my_strlen2(char* arr)
//{
//	assert(arr != NULL);
//	if (*arr == '\0')
//		return 0;
//	else
//	{
//		return 1 + my_strlen2(arr + 1);
//	}
//}
//int my_strlen3(char* arr)
//{
//	assert(arr != NULL);
//	char* p = arr;
//	while (*p != '\0')
//	{
//		p++;
//	}
//	return p - arr;
//}
//int main()
//{
//	char arr[] = "abcd";
//	int len = 0;
//	//len = my_strlen1(arr);
//	//len = my_strlen2(arr);
//	len = my_strlen3(arr);
//	printf("%d\n", len);
//	return 0;
//}


//#include <stdio.h>
//#include <assert.h>
//void my_strcmp(char* dest, const char* src)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	while (*src)
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//}
//int main()
//{
//	char arr[20] = "abcd";
//	char arr2[20] = "0";
//	my_strcmp(arr2, arr);
//	printf("%s\n", arr2);
//	return 0;
//}

//#include <stdio.h>
//#include <assert.h>
//void my_strcat(char* dest, const char* src)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	while (*dest)
//	{
//		dest++;  // 找到arr1数组中的最后一个元素的后一位
//	}
//	while (*src)
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//}
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[20] = "world";
//	my_strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}


//#include <stdio.h>
//#include <assert.h>
//void* my_strstr(char* dest, const char* src)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	char* s1 = NULL;
//	char* s2 = NULL;
//	char* ps = dest;
//
//	while (*src)
//	{
//		s1 = ps;
//		s2 = src;
//		while (*s1 == *s2 && *s1 && *s2)
//		{
//			s1++;
//			s2++;
//			if (*s2 == '\0')
//			{
//				return ps;
//			}
//		}
//		ps++;
//	}
//	return NULL;
//}
//int main()
//{
//	char arr1[20] = "abababababcedf";
//	char arr2[20] = "abc";
//	char* ps = NULL;
//	ps = my_strstr(arr1, arr2);
//	printf("%s\n", ps);
//	return 0;
//}


//#include <stdio.h>
//#include <assert.h>
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	char* ret = dest;
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	int ar[8] = { 0 };
//	my_memcpy(ar, arr, 20);
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", ar[i]);
//	}
//	return 0;
//}

//#include <stdio.h>
//#include <assert.h>
//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	char* ret = dest;
//	if (dest < src)
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int ar[10] = { 0 };
//	my_memmove(arr, arr + 2, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}


#include <stdio.h>
#include <assert.h>
int my_strcmp(const char* str1, const char* str2)
{
	assert(str1 && str2);
	while (*str1 == *str2) 
	{
		if (*str1 == '\0')
			return 0;
		str1++;
		str2++;
	}
	return *str1 - *str2;
}
int main()
{
	char arr1[] = "abzqw";
	char arr2[] = "abq";
	if (strcmp(arr1, arr2) > 0)
		printf(">\n");
	else if (strcmp(arr1, arr2) == 0)
		printf("=\n");
	else
		printf("<\n");
	return 0;
}

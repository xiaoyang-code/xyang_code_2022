#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)
//
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//

//int main()
//{
//	//打开文件
//	char arr[20] = "abcdefg";
//	FILE* pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写文件
//	fwrite(arr, sizeof(arr), 1, pf);
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}

//int main()
//{
//	char arr[20] = { 0 };
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	char tmp = fgetc(pf);//a
//	printf("%c ", tmp);
//	tmp = fgetc(pf);//b
//	printf("%c ", tmp);
//	tmp = fgetc(pf);//c
//	printf("%c ", tmp);
//	tmp = fgetc(pf);//d
//	printf("%c ", tmp);
//	
//	fseek(pf, -3, SEEK_CUR);
//	tmp = fgetc(pf);
//	printf("%c ", tmp);//b
//
//	rewind(pf);
//	printf("%d ", ftell(pf));
//	tmp = fgetc(pf);
//	printf("%c ", tmp);
//	/*while (fread(arr, sizeof(char), 1, pf))
//	{
//		printf("%s\n", arr);
//	}*/
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//int main()
//{
//	int a = 10000;
//	FILE* pf = fopen("test.txt", "wb");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fwrite(&a, sizeof(a), 1, pf);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//int main()
//{
//	//feof()用法
//	int arr[5] = { 1,2,3,4,5 };
//	FILE* pf = fopen("test.txt", "w");
//	if (!pf)
//	{
//		perror("write::fopen");
//		return EXIT_FAILURE;
//	}
//	fwrite(&arr, sizeof(int), 5, pf);
//	fclose(pf);
//	pf = NULL;
//
//	int brr[5] = { 0 };
//	pf = fopen("test.txt", "r");
//	size_t ret_read = fread(&brr, sizeof(int), 5, pf);
//	if (!pf)
//	{
//		perror("read::fopen");
//		return EXIT_FAILURE;
//	}
//	if (ret_read == 5)
//	{
//		for (int i = 0; i < 5; i++)
//		{
//			printf("%d\n", brr[i]);
//		}
//	}
//	else
//	{
//		//错误判断
//		if (ferror(pf))
//		{
//			printf("I/O error when reading\n");
//		}
//		else if (feof(pf))
//		{
//			printf("End of file reached successfully\n");
//		}
//	}
//	return 0;
//}



//实现一个代码，拷贝一个文件
//int main()
//{
//
//	//打开文件
//	FILE* pfRead = fopen("test.txt", "r");
//	if (pfRead == NULL)
//	{
//		perror("pfRead");
//		return 1;
//	}
//
//	FILE* pfWrite = fopen("test.txt", "w");
//	if (pfWrite == NULL)
//	{
//		fclose(pfRead);
//		pfRead = NULL;
//		perror("pfWrite");
//		return 1;
//	}
//
//	//拷贝内容
//	int ch = 0;
//	while ((ch = fgetc(pfRead)) != EOF)
//	{
//		fputc(ch, pfWrite);
//	}
//
//	//关闭文件
//	fclose(pfRead);
//	fclose(pfWrite);
//	pfRead = NULL;
//	pfWrite = NULL;
//	return 0;
//}


//int main()
//{
//	//C语言中，会天然的将两个字符串合并成一个字符串
//	printf("hello " "world" "\n");
//	printf("%s\n", "hello" "world\n");
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	printf("the value of "num"is""%d\n", a);
//
//	return 0;
//}

#include "test.h"
#include <stdio.h>

//extern int Add(int, int);
//
//int main()
//{
//	int ret = Sub(3, 5);
//	printf("%d ", ret);
//
//	ret = Add(3, 5);
//	printf("%d ", ret);
//
//	printf("%s %s %s %d\n", __FILE__, __DATE__, __TIME__, __LINE__);
//	__FILE__;
//	__DATE__;
//	__TIME__;
//	__LINE__;
//
//	return 0;
//}


#define print_format(num,type)  printf("the value of "#num" is "type,num)
int main()
{
	int a = 10;
	print_format(a, "%d\n");

	int b = 20;
	print_format(b, "%d\n");


	float f = 3.14f;
	print_format(f, "%.2f\n");

	return 0;
}
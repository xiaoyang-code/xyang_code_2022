#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

/*-------------------------------------------------------------------------
	File description: operations for linklist
	Author: liuxin_dz
	Date: 2023-2-27
-------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define STATISTIC (0)	//性能统计时，编译开关打开，不进行printlist操作
#define PRINT_NUM (10)	//一次打印链表中结点个数

typedef struct tagLNode
{
	int data;
	struct tagLNode* next;	//结点指针，递归定义
}LNode, * PLNode, * LinkList;

void printList(LinkList L, int song_id);

/*   I N I T _   L I N K   L I S T   */
/*-------------------------------------------------------------------------
	Description: initiate a null linklist with a header link node
	Input: LinkList *L
	Output: LinkList *L
	Result: 0: success
			Others:fail
-------------------------------------------------------------------------*/
int init_LinkList(LinkList* L)
{
	assert(NULL != L);

	PLNode s = (PLNode)malloc(sizeof(LNode));
	if (!s)	//判空
		return -1;

	//头结点初始化
	s->next = NULL;
	*L = s;

	return 0;
}


/*   L O C A T E   L I N K   L I S T   */
/*-------------------------------------------------------------------------
	Description: locate data x in the linklist L
	Input: LinkList L,int x
	Result: NULL: no data x in the linklist
			Others:retun the pointer of the node x
-------------------------------------------------------------------------*/
PLNode Locate_LinkList(LinkList L, int x)
{
	assert(NULL != L);

	/*在单链表L中查找值为x的结点，找到后返回其指针，否则返回空*/
	PLNode p = L->next;	//L为带头结点的单链表

	while (p && p->data != x)
		p = p->next;

	return p;
}

/*-------------------------------------------------------------------------
	Description: 返回链表中值为x的位置，第一个结点（非头结点）返回1
				与Locate_LinkList的区别：本函数返回的是链表中的结点顺序号，
				不是指针。
	Input: LinkList L,int x
	Result: x的位置
-------------------------------------------------------------------------*/
int Search_LinkList(LinkList L, int x)
{
	int pos = 0;
	assert(NULL != L);

	/*在单链表L中查找值为x的结点，找到后返回其指针，否则返回空*/
	PLNode p = L->next;	//L为带头结点的单链表
	while (p && p->data != x)
	{
		p = p->next;
		pos++;
	}

	if (!p)
		return -1;	//找不到x，返回-1 
	else
		return (pos + 1);
}

/*   G E T _   L I N K   L I S T   */
/*-------------------------------------------------------------------------
	Description: 从链表L中获取第i个结点的指针,规定链表的第0个结点为头结点
	Input: LinkList L,int i: the postion of the node in the linklist
	Result: NULL: no ith node
			Others:return the ith node
-------------------------------------------------------------------------*/
PLNode Get_LinkList(LinkList L, int i)
{
	PLNode p = L;
	int j = 0;

	assert(NULL != L);

	while (p->next != NULL && j < i)
	{
		p = p->next;
		j++;
	}

	if (j == i)
		return p;
	else
		return NULL;
}


/*   D E S T R O Y _   L I N K   L I S T   */
/*-------------------------------------------------------------------------
	Description: 销毁链表
	Input: LinkList L
-------------------------------------------------------------------------*/
void Destroy_LinkList(LinkList L)
{
	PLNode p = NULL;

	assert(NULL != L);

	p = L->next;

	while (p)
	{
		L->next = p->next;	//将p摘链
		free(p);
		p = L->next;
	}

	free(L);
}


/*   I N S E R T    L I N K   L I S T   */
/*-------------------------------------------------------------------------
	Description:将数据元素x插入到链表的第i个位置
	Input: LinkList L, int i: insert position, int x
	Result: 0: success
			-1:fail
-------------------------------------------------------------------------*/
int Insert_LinkList(LinkList L, int i, int x)
{
	assert(NULL != L);

	PLNode p = Get_LinkList(L, i - 1);	//查找第i-1个结点
	if (!p)
		return -1;

	PLNode s = (PLNode)malloc(sizeof(LNode));
	if (!s)	//判空
		return -1;

	//结点初始化
	s->next = NULL;
	s->data = x;

	//将结点s插到结点p的后面
	s->next = p->next;
	p->next = s;
	return 0;
}


/*   D E L _   L I N K   L I S T   */
/*-------------------------------------------------------------------------
	Description:删除链表中值为x的结点，注意头结点是第0个结点
	Input: LinkList L, int x
	Result: 0: success
			-1:fail
-------------------------------------------------------------------------*/
int Del_LinkList(LinkList L, int x)
{
	PLNode p = NULL;
	PLNode q = NULL;

	assert(NULL != L);

	//q是p的前驱结点
	p = L->next;
	q = L;

	while (p && p->data != x)
	{
		//p和q同时向后移动一个位置
		q = p;
		p = p->next;
	}

	if (!p)
		return -1;	//找不到值为x的结点
	
	q->next = p->next;//删除结点p
	free(p);	//释放结点s的资源

	printList(L, q->data);
	return 0;
}

/*   L E N G T H _   L I N K   L I S T   */
/*-------------------------------------------------------------------------
	Description:求表长
	Input: LinkList L
	Result: 表长
-------------------------------------------------------------------------*/
int Length_LinkList(LinkList L)
{
	PLNode p = NULL;
	int len = 0;

	assert(NULL != L);

	p = L->next;
	while (p)
	{
		len++;
		p = p->next;
	}

	return len;
}

#if STATISTIC
/*   P R I N T   L I S T   */
/*-------------------------------------------------------------------------
	Description:性能统计时，关闭打印信息
	Input: LinkList L, int song_id
	Result: void
-------------------------------------------------------------------------*/
void printList(LinkList L, int song_id)
{

}
#else 
/*   P R I N T   L I S T   */
/*-------------------------------------------------------------------------
	Description:打印链表，每次打印song_id前后共PRINT_NUM个结点
	Input: LinkList L, int song_id
	Result: void
-------------------------------------------------------------------------*/
void printList(LinkList L, int song_id)
{
	assert(NULL != L);

	PLNode p = NULL;
	int start = 0;	//打印的起始位置
	int end = 0;	//打印的结束位置
	int i = 0;

	int len = Length_LinkList(L);
	int pos = Search_LinkList(L, song_id);
	if (pos < 1)
		return;

	end = pos + PRINT_NUM / 2;
	start = pos - PRINT_NUM / 2 + 1;

	if (start < 1)
	{
		start = 1;
		end = PRINT_NUM;
	}

	if (end > len)
	{
		end = len;
		start = len - PRINT_NUM + 1;
	}

	if (start < 1)
		start = 1;

	for (i = start; i <= end; i++)
		printf("%4d  ", i);
	printf("\r\n");


	p = L->next;
	i = 0;
	while (p)
	{
		i++;
		if (i >= start && i <= end)
			if (!p->next)
				printf("%4d^", p->data);
			else
				printf("%4d->", p->data);
		p = p->next;
	}
	printf("\r\n");
}
#endif

int main()
{
	int choice;
	int song_id = -1;
	int rst = -1;
	LinkList L = NULL;
	PLNode pLNode = NULL;

	//初始化歌曲列表资源
	rst = init_LinkList(&L);
	if (0 != rst)
		return rst;

	while (1) {
		printf("---------link list operations---------\r\n");
		printf("0:退出\r\n");
		printf("1:添加歌曲\r\n");
		printf("2:删除歌曲\r\n");
		printf("3:查找歌曲\r\n");
		printf("4:歌曲总数\r\n");

		scanf("%d", &choice);
		switch (choice) {
		case 1:
			printf("请输入待添加歌曲曲号\r\n");
			scanf("%d", &song_id);
			pLNode = Locate_LinkList(L, song_id);
			if (NULL != pLNode) {
				printf("待添加歌曲%d已存在\r\n", song_id);
				continue;
			}

			//为了提高添加效率，在链表头部插入结点
			rst = Insert_LinkList(L, 1, song_id);
			if (0 == rst) {
				printf("歌曲%d添加成功\r\n", song_id);
				printList(L, song_id);
			}
			else
				printf("歌曲%d添加失败\r\n", song_id);
			break;
		case 2:
			printf("请输入待删除歌曲曲号\r\n");
			scanf("%d", &song_id);

			rst = Del_LinkList(L, song_id);
			if (0 != rst)
				printf("歌曲%d在列表中不存在\r\n", song_id);
			else {
				printf("歌曲%d删除成功\r\n", song_id);
			}

			break;
		case 3:
			printf("请输入待查找歌曲曲号\r\n");
			scanf("%d", &song_id);
			pLNode = Locate_LinkList(L, song_id);
			if (NULL != pLNode) {
				printf("歌曲%d查找成功\r\n", song_id);
				printList(L, song_id);
			}
			else
				printf("歌曲%d查找失败\r\n", song_id);
			break;
		case 4:
			printf("列表中歌曲数目%4d\r\n", Length_LinkList(L));
			if (L->next)
				printList(L, L->next->data);
			break;
		case 0:
		default:
			printf("退出\r\n");
			Destroy_LinkList(L);	//释放歌曲列表资源
			return 0;

		}
	}
	return 0;
}

#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>


//指针和数组不同
//指针是存放地址的变量，数组名指的是数组首元素的地址，所以能存放在指针变量中，
//也可以通过指针来对数组进行修改。
//数组名大部分情况下指的是受阻首元素的地址
//但也有2个例外：
//1. sizeof（数组名），是整个数组的大小，不是数组首元素的的大小 
//2. 
//一维数组
int main()
{
	int a[] = { 1,2,3,4 };
	printf("%d\n", sizeof(a));
	{
		//
	}
	printf("%d\n", sizeof(a + 0));
	printf("%d\n", sizeof(*a));
	printf("%d\n", sizeof(a + 1));
	printf("%d\n", sizeof(a[1]));
	printf("%d\n", sizeof(&a));
	printf("%d\n", sizeof(*&a));
	printf("%d\n", sizeof(&a + 1));
	printf("%d\n", sizeof(&a[0]));
	printf("%d\n", sizeof(&a[0] + 1));
	return 0;
}
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int my_strlen(char* str)
//{
//	char* start = str;
//	char* end = str;
//	while ( * end != '\0')
//	{
//		end++;
//	}
//	return end - start;
//
//}
//
//int main()
//{
//	char arr[] = "bit";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

//int main()
//{
//	int arr1[10] = { 0 };
//	int i = 0;
//	int* parr1 = arr1;
//	for (i = 0; i < 10; i++)
//	{
//		*(parr1 + i) += i;
//		printf("%d ", arr1[i]);
//
//	}
//	printf("\n");
//
//	int arr2[10] = { 0 };
//	char* parr2 = arr2;
//	for (i = 0; i < 10; i++)
//	{
//		*(parr2 + i) += i;
//		printf("%d ", arr2[i]);
//
//	}
//
//	return 0;
//
//}
//
//int main()
//{
//	int a = 10;
//	int* p = NULL;
//	//NULL - 相当于\0 
//	*p = &a;
//	return 0;
//
//}

//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	int i = 0;
//	for (i = 0; i < 12; i++)
//	{
//		*(p + i) = i;
//		printf("%d ", arr[i]);
//	}
//}
//
//int main()
//{
//	int* p = NULL;
//	//....
//	int a = 10;
//	p = &a;
//	if (p != NULL)//检查指针有效性
//	{
//		*p = 20;
//	}
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	int** ppa = &pa;
//	printf("pa = %p\n", pa);
//	printf("pa = %d\n", *pa);
//
//	printf("ppa = %p\n", ppa);
//	printf("ppa = %d\n", **ppa);
//
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 0 };
//	int* pa = arr;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(pa + i) = i;
//		printf("arr[%d] = %d : %p\n",i,arr[i], pa + i);
//	}
//
//	int* vp = NULL;
//	int arr[10] = { 0 };
//	for (vp = &arr[10]; vp > &arr[0];vp--)
//	{
//		*vp = 0;
//
//	}
//	return 0;
//}

int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int* pa = arr;
	printf("%p\n", arr);
	printf("%p\n",&arr[0]);
	return 0;
}
#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>
#include <string.h>
#include <assert.h>

//int int_cmp(const void* str1, const void* str2)
//{
//	return (*(int*)str1) - (*(int*)str2);
//}
//void swap(char* buf1, char* buf2, size_t width)
//{
//	size_t i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//		
//	}
//}
//void my_qsort(void* base,size_t count,size_t width,int (*cmp)(void* ,void*) )
//{
//	size_t i = 0;
//	for (i = 0; i < count - 1; i++)
//	{
//		size_t j = 0;
//		for (j = 0; j < count - i - 1; j++)
//		{
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
//			{
//				swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
//			}
//		}
//	}
//}
//int main()
//{
//	/*strlen();
//	strcat();
//	strcmp();
//	strcpy();
//	strncmp();
//	strncat();
//	strncpy();
//	strstr();
//	strstr();
//	strchr();
//	strrchr();*/
//	int arr[] = { 9,8,7,6,5,4,3,2,1 };
//	my_qsort(arr,sizeof(arr)/sizeof(arr[0]),sizeof(int),int_cmp);
//	for (int i = 0; i < 9; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//char* my_strstr(const char* dest, const char* src)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	char* ps = (char*)dest;
//	char* s1 = NULL;
//	char* s2 = NULL;
//
//	while (*ps)
//	{
//		s1 = ps;
//		s2 = (char*)src;
//		while(*s1 && *s2 && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return ps;
//		}
//		ps++;
//	}
//	return NULL;
//
//}
//int main()
//{
//	char arr[] = "this is a hanhandsome ";
//	char* p = NULL;
//	p = my_strstr(arr,"hand");
//	printf("%s\n", p);
//	return 0;
//}

//int main()
//{
//	char arr[] = "193.423.5.321";
//	char buf[30] = "0";
//	strcpy(buf, arr);
//	const char* p = ".";
//	char* str = NULL;
//	for (str = strtok(buf, p); str != NULL; str = strtok(NULL, p))
//	{
//		printf("%s\n", str);
//	}
//	return 0;
//}


//int main()
//{
//	char arr[] = "943.23.4734.4";
//	char* p = ".";
//	char* cp = NULL;
//	for (cp = strtok(arr, p); cp != NULL; cp = strtok(NULL, p))
//	{
//		printf("%s\n", cp);
//	}
//	return 0;
//}

//#include <errno.h>
//int main()
//{
//	//打开文件
//	//打开文件的时候，如果文件的打开方式是"r"
//	//文件存在则打开成功，文件不存在打开失败
//	//打开文件失败的话，会返回NULL
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		perror("打开文件失败");
//		//perror  == strerror + printf;
//
//		//printf("%s",strerror(errno));
//		return 1;
//	}
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//
//	char* ret = dest;
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	int ar[8] = { 0 };
//	//memcpy(ar, arr, 20);
//	my_memcpy(ar, arr, 20);
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", ar[i]);
//	}
//	return 0;
//}
void* my_memmove(void* dest, const void* src, size_t num)
{
	assert(dest != NULL);
	assert(src != NULL);

	char* ret = dest;
	if (dest < src)
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	return ret;
}
test1()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int ar[10] = { 0 };
	my_memmove(arr, arr+2, 20);
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
}

#include <ctype.h>
test2()
{
	char arr[20] = { 0 };
	gets(arr);
	int i = 0;
	for (i = 0; i < 20; i++)
	{
		if (toupper(arr[i]))
		{
			arr[i] = tolower(arr[i]);
		}
		printf("%c", arr[i]);
	}

}
int main()
{
	test1();
	//test2();
	return 0;
}


#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"

void init_board(char board[ROW][COL], int row, int col)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			board[i][j] = '0';
		}
	}
}

void print_board(char board[ROW][COL], int row, int col)
{
	printf("*******************开始游戏********************\n");
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			printf(" %c ", board[i][j]);
			if (j < col - 1)
			{
				printf("|");
			}
		}
		printf("\n");
		for (j = 0; j < col; j++)
		{
			printf("---");
			if (j < col - 1)
			{
				printf("|");
			}
		}
		printf("\n");
	}
	printf("*******************开始游戏********************\n");
}

void paly_board(char board[ROW][COL], int row, int col)
{
	int x = 0;
	int y = 0;
	while (1)
	{
		printf("请输入坐标:>");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (board[x - 1][y - 1] == '0')
			{
				board[x - 1][y - 1] = '*';
				break;
			}
		}
		else
		{
			printf("输入错误，请重新输入。");
		}
	}
}

void computer_board(char board[ROW][COL], int row, int col)
{
	while (1)
	{
		int x = rand() % row ;
		int y = rand() % col ;
		if (board[x][y] == '0')
		{
			board[x][y] = '#';
			break;
		}
	}
}

char is_win(char board[ROW][COL], int row, int col)
{
	if (board[0][0] == board[1][1] && board[2][2] == board[0][0] && board[0][0] != '0')
	{
		return board[0][0];
	}
	else if (board[0][0] == board[0][1] && board[0][2] == board[0][0] && board[0][0] != '0')
	{
		return board[0][0];
	}
	else if (board[0][0] == board[1][0] && board[2][0] == board[0][0] && board[0][0] != '0')
	{
		return board[0][0];
	}
	else if (board[0][2] == board[1][1] && board[2][0] == board[2][0] && board[2][0] != '0')
	{
		return board[2][0];
	}
	else
	{
		return 'C';
	}
}
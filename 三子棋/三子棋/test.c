#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

void menu()
{
	printf("*****************************************\n");
	printf("******  1 . play    0 . exit    *********\n");
	printf("*****************************************\n");
}

void game()
{
	char board[ROW][COL] = { 0 };
	char ret = "0";
	//初始化
	init_board(board, ROW, COL);
	//打印
	print_board(board, ROW, COL);
	while (1)
	{
		//玩家下
		paly_board(board, ROW, COL);
		ret = is_win(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}

		//电脑下
		computer_board(board, ROW, COL);
		ret = is_win(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}

		print_board(board, ROW, COL);
	}
	switch (ret)
	{
	case '*':
		printf("玩家胜！\n");
		print_board(board, ROW, COL);
		break;
	case '#':
		printf("电脑胜！\n");
		print_board(board, ROW, COL);
		break;
	case 'Q':
		printf("平局！\n");
		print_board(board, ROW, COL);
		break;
	}
}
int main()
{
	int input = 0;
	srand((unsigned)time(NULL));
	do
	{
		menu();
		printf("请输入:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("游戏结束！\n");
		default:
			printf("输入错误，请重新输入。\n");
		}
	} while (input);

	return 0;
}
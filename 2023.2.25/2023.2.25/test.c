#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>
#include <assert.h>
#include <string.h>

int revolve( char ps[], const char s2[], size_t sz)
{
	assert(ps != NULL);
	assert(s2 != NULL);

	
	int flag = 1;  // 1就不是   0就是
	while (1 == flag)
	{
		
		for (int i = 0; i < sz; i++)
		{
			char tmp = ps[i];
			ps[i] = ps[sz];
			ps[sz] = tmp;

			for (int j = 1; j <= sz; j++)
			{
				char tmpp = ps[i];
				ps[i] = ps[j];
				ps[j] = tmpp;
			}

			int len = strcmp(ps, s2);
			if (0 == len)
			{
				flag = 0;
				break;
			}
		}
	}

	return flag;
}
int main()
{
	char s1[] = "abcd";
	char s2[] = "cdab";
	size_t sz = strlen(s1);
	char ps[] = { 0 };
	strcpy(ps, s1);

	int len = revolve(ps,s2,sz);

	if (len == 0)
	{
		printf("%s是%s旋转后的结果。\n", s2, s1);

	}
	else
	{
		printf("%s不是%s旋转后的结果。\n", s2, s1);
	}
	return 0;
}
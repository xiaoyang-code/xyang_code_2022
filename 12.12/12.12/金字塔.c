#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main()
{
    char a;
    scanf("%c", &a);
    int n = 5;
    for (int i = 1; i <= n / 2; i++)
    {
        for (int j = i; j <= n / 2; j++)
            putchar(' ');
        for (int j = 1; j <= 2 * i - 1; j++)
            putchar(a);

        printf("\n");
    }
    for (int i = 1; i <= n; i++)
        putchar(a);
    printf("\n");
    for (int i = 1; i <= n / 2; i++)
    {
        for (int j = 1; j <= i; j++)
            putchar(' ');
        for (int j = 1; j <= n - 2 * i; j++)
            putchar(a);

        printf("\n");
    }


    return 0;
}
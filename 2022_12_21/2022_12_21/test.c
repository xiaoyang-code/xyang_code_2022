#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//编写一个函数实现计算字符串长度 —— 2种方法
//int my_strlen(char* arr)
//{
//	int count = 0;
//	while (*arr != '\0')
//	{
//		count++;
//		arr++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "123456789";
//	int len = my_strlen(arr);
//	printf("%d", len);
//
//	return 0;
//}

//int my_strlen(char* arr)
//{
//	if (*arr != '\0')
//		return 1 + my_strlen(arr+1);
//}
//int main()
//{
//	char arr[] = "12456";
//
//	printf("%d\n", my_strlen(arr));
//
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6 };
//	int* pa = &arr;
//	printf("%d\n", *pa+2);
//	printf("%d\n", *(++pa));
//	printf("%d\n", *pa);
//	
//	return 0;
//
//}

//编写一个函数实现使字符串倒序储存
//常规法
//int my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//void reverse_string(char* str)
//{
//	int left = 0;
//	int right = my_strlen(str) - 1;
//
//	while (left < right)
//	{
//		/*char tmp = str[left];
//		str[left] = str[right];
//		str[right] = tmp;
//		left++;
//		right--;*/
//
//		char tmp = *(str + left);
//		*(str + left) = *(str + right);
//		*(str + right) = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	char arr[] = "123456";
//	reverse_string(arr);
//	printf("%s", arr);
//	return 0;
//}

//递归法
//int my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//void reverse_string(char* str)
//{
//	char tmp = *str;
//	int len = my_strlen(str);
//	*str = *(str + len - 1);
//	*(str + len - 1) = '\0';
//	if(my_strlen(str+1)>=2)
//		reverse_string(str + 1);
//	*(str + len - 1) = tmp;
//}
//int main()
//{
//	char arr[] = "123456";
//	reverse_string(arr);
//	printf("%s\n", arr);
//}

//编写一个函数实现每一位数相加，使用递归
//int digitsum(int n)
//{
//	if (n > 9)
//	{
//		return digitsum(n/10) + n % 10;
//	}
//	else
//	{
//		return n;
//	}
//}
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int sum = digitsum(a);
//	printf("%d\n", sum);
//	return 0;
//}

//编写一个函数实现n的k次方，使用递归

//int pow(int n, int k)
//{
//	if (k == 0)
//	{
//		return 1;
//	}
//	else if (k > 0)
//	{
//		return pow(n,k-1) * n;
//	}
//}
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	int ret = pow(n, k);
//	printf("%d\n", ret);
//	return 0;
//}























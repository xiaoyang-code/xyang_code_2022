#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)
/*-------------------------------------------------------------------------
	Description: stack operations
	Author: liuxin_dz
	Date: 2023-03-22
-------------------------------------------------------------------------*/
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define  MAXSIZE  (10)

typedef char* datatype;

//每层添加的PDU
static char* g_layer_pdu[] = { "{PDU5}","{PDU4}","{PDU3}","{PDU2}","{PDU1}" };

//存放封装好的数据
static char g_send_str[128] = { '\0' };

typedef  struct
{
	datatype  data[MAXSIZE];
	int  top;
}SeqStack; //顺序栈的类型的定义 

/*   I N I T _   S E Q   S T A C K   */
/*-------------------------------------------------------------------------
	Description: 置空栈算法，初始化栈
	Result：返回顺序栈的指针SeqStack*
-------------------------------------------------------------------------*/
SeqStack* Init_SeqStack()
{
	SeqStack* s;
	s = (SeqStack*)malloc(sizeof(SeqStack)); /*申请栈空间*/
	if (!s)
	{
		printf("空间不足\r\n");
		return NULL;     /*未申请到足够大的存储空间，返回空指针*/
	}
	else
	{
		s->top = -1;         /*初始化栈顶指针*/
		return s;             /*申请到栈空间，返回栈空间地址*/
	}
}

/*   D E S T R O Y _   S E Q   S T A C K   */
/*-------------------------------------------------------------------------
	Description: 销毁顺序栈
	Input: SeqStack *s：顺序栈指针
	Result：void
-------------------------------------------------------------------------*/
void Destroy_SeqStack(SeqStack* s)
{
	assert(NULL != s);

	free(s);
	s = NULL;
}

/*   E M P T Y _   S E Q   S T A C K   */
/*-------------------------------------------------------------------------
	Description: 判栈空算法
	Input: SeqStack *s 顺序栈指针
	Result：1：空栈
			0：非空栈
-------------------------------------------------------------------------*/
int Empty_SeqStack(SeqStack* s)
{
	assert(NULL != s);

	if (-1 == s->top)
		return 1;    /*空栈返回1*/
	else
		return 0;
}

/*   C L E A R _   S E Q   S T A C K   */
/*-------------------------------------------------------------------------
	Description: 清空栈
	Input: SeqStack *s 顺序栈指针
	Result：void
-------------------------------------------------------------------------*/
void clear_SeqStack(SeqStack* s)
{
	assert(NULL != s);
	s->top = -1;
}

/*   P U S H _   S E Q   S T A C K   */
/*-------------------------------------------------------------------------
	Description: 入栈算法
	Input: SeqStack *s：顺序栈指针，datatype x: 入栈元素
	Result：0：操作成功
			-1：操作失败
-------------------------------------------------------------------------*/
int Push_SeqStack(SeqStack* s, datatype  x)
{
	assert(NULL != s);

	if (MAXSIZE - 1 == s->top)
		return -1;         /*栈满不能入栈，返回错误代码-1*/
	else
	{
		s->top++;       /*栈顶指针向上移动*/
		s->data[s->top] = x; /*将x置入新的栈顶*/
		return 0;                 /*入栈成功，返回成功代码0 */
	}
}

/*   P R I N T _   S T A C K  */
/*-------------------------------------------------------------------------
	Description: 打印栈内当前的元素
	Input: SeqStack *s：顺序栈指针
	Result: void
-------------------------------------------------------------------------*/
void Print_Stack(SeqStack* s)
{
	int i;
	assert(NULL != s);

	printf("\r\n");

	for (i = 0; i <= s->top; i++)
		printf("%s", s->data[i]);
}

/*   P O P _   S E Q   S T A C K   */
/*-------------------------------------------------------------------------
	Description: 出栈算法
	Input: SeqStack *s：顺序栈指针, datatype *x: 出栈元素的指针
	Result：0：操作成功
			-1：操作失败
-------------------------------------------------------------------------*/
int Pop_SeqStack(SeqStack* s, datatype* x)
{
	assert(NULL != s);

	/*通过*x返回原栈顶元素*/
	if (Empty_SeqStack(s))
		return -1;       /*栈空不能出栈，返回错误代码-1*/
	else
	{
		*x = s->data[s->top];   /*保存栈顶元素值*/
		s->top--;     /*栈顶指针向下移动*/
		return 0;     /*返回成功代码0 */
	}
}

/*   T O P _   S E Q   S T A C K   */
/*-------------------------------------------------------------------------
	Description: 取栈顶元素算法
	Input: SeqStack *s：顺序栈指针,datatype *x: 栈顶元素的指针
	Result：0：操作成功
			-1：操作失败
-------------------------------------------------------------------------*/
int Top_SeqStack(SeqStack* s, datatype* x)
{
	assert(NULL != s);

	/*仅是读取栈顶元素，栈顶指针不发生变化*/
	/*栈顶元素可以通过参数返回*/
	if (Empty_SeqStack(s))
		return -1; /*栈空无元素*/
	else
	{
		*x = s->data[s->top];
		return 0;
	}
}

/*   P U S H   P D U   S T A C K   */
/*-------------------------------------------------------------------------
	Description: 每层PDU依次进栈
	Input: SeqStack *s：顺序栈指针
	Result：void
-------------------------------------------------------------------------*/
inline void PushPduStack(SeqStack* s)
{
	int i = 0;
	assert(NULL != s);

	for (i = 0; i < sizeof(g_layer_pdu) / sizeof((g_layer_pdu)[0]); i++)
		Push_SeqStack(s, g_layer_pdu[i]);
}

int main(void)
{
	int choice;
	datatype top_str;
	char app_str[64] = { '\0' };

	SeqStack* s = Init_SeqStack();
	if (NULL == s)
		return -1;

	while (1)
	{

		printf("---------operations---------\r\n");
		printf("0:退出\r\n");
		printf("1:封装协议数据\r\n");

		scanf("%d", &choice);
		switch (choice) {
		case 1:
			printf("输入字符串：");
			scanf("%s", app_str);

			//清空栈
			clear_SeqStack(s);
			g_send_str[0] = '\0';

			Push_SeqStack(s, app_str);
			PushPduStack(s);

			while (!Empty_SeqStack(s))
			{
				Pop_SeqStack(s, &top_str);
				strcat(g_send_str, top_str);

			}

			printf("处理后的字符串为%s\r\n", g_send_str);
			break;
		case 0:
		default:
			Destroy_SeqStack(s);
			break;
		}

	}

}

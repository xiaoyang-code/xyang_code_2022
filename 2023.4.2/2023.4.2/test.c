#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)


#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#define PRINT_NUM (10) //每次打印10个数据

#define ARRAY_SIZE (100)
#define NULL ((void *)0) 

typedef  struct {
	int  data[ARRAY_SIZE];
	int  last;
} SeqList;


/*   I N I T   S E Q   L I S T   */
/*-------------------------------------------------------------------------
【算法2-1】顺序表初始化
	Return: SeqList*
-------------------------------------------------------------------------*/
SeqList* initSeqList()
{
	SeqList* L;
	L = (SeqList*)malloc(sizeof(SeqList));/*申请顺序表的存储空间*/
	if (L)
	{
		L->last = -1;	//初始表元素个数为0
		return L;
	}/*返回顺序表的存储地址*/
	else
		return NULL;/*申请不成功，返回错误代码-1*/
}


/*   D E S T R O Y   S E Q   L I S T   */
/*-------------------------------------------------------------------------
	Description:顺序表释放
	Return:
-------------------------------------------------------------------------*/
void destroySeqList(SeqList* L)
{
	assert(NULL != L);

	free(L);
}


/*   L O C A T I O N    S E Q   L I S T   */
/*-------------------------------------------------------------------------
   【算法2-4】 顺序表的按值查找算法 遍历
	Return: operation state
			-1: fail
			others:sucess: the location of the x in the seqList
-------------------------------------------------------------------------*/
int LocationSeqList(SeqList* L, int x)
{
	int i = 0;
	assert(NULL != L);

	while (i <= L->last && L->data[i] != x) //顺序检查数据元素值
		i++;

	if (i > L->last) //到最后元素，没有找到
		return -1;//查找不成功，返回错误代码-1
	else
		return i; //查找成功，返回数据元素在顺序表的存储位置
}


/*   I N S E R T    S E Q   L I S T   */
/*-------------------------------------------------------------------------
	【算法2-2】 顺序表的插入算法
	IN: SeqList *L,int i,datatype x 1<=i<=L.length()+1
	Return: operation result
			0: success
			others:fail
-------------------------------------------------------------------------*/
int insertSeqList(SeqList* L, int i, int x)
{
	int j;

	assert(NULL != L);

	if (i<1 || i>L->last + 2) /*检查插入位置的正确性*/
	{
		printf("位置错");
		return -1;/*插入位置参数错，返回错误代码0 */
	}

	if (L->last == ARRAY_SIZE - 1)
	{
		printf("表满");
		return -1;/*表空间已满，不能插入，返回错误代码-1*/
	}

	for (j = L->last; j >= i - 1; j--)
	{
		int tmp = L->data[j];
		L->data[j] = L->data[j + 1];
		L->data[j + 1] = tmp;
	}/*元素向后移动一个位置 */

	L->data[i - 1] = x;	/*新元素插入*/
	L->last++;/*last指向新的最后元素*/

	return 0;	/*插入成功，返回成功代码1 */
}


/*   D E L E T E    S E Q   L I S T   */
/*-------------------------------------------------------------------------
   【算法 2-3】 顺序表的删除算法
	IN: SeqList *L,int i,1<=i<=L.length()
	Return: operation result
	0: success
	others:fail
-------------------------------------------------------------------------*/
int deleteSeqList(SeqList* L, int i)
{
	int j;

	assert(NULL != L);

	if (i<1 || i>L->last + 1)/*检查空表及删除位置的合法性*/
	{
		printf("不存在第i个元素");
		return -1;         /*不能删除，返回错误代码0*/
	}

	for (j = i; j <= L->last; j++)
	{
		int tmp = L->data[j];
		L->data[j] = L->data[j - 1];
		L->data[j - 1] = tmp;
	}    /*数据元素向前移动*/

	L->last--; 		/* last指向新的最后元素*/

	return 0; 		/*删除成功，返回成功代码1*/
}

/*   P R I N T   L I S T   */
/*-------------------------------------------------------------------------
	Description: print song list , print 10 items in one time
	Input: SeqList* L: the song list, int i: the position of the song
		   in the array.
	Result:
-------------------------------------------------------------------------*/
void printList(SeqList* L, int i)
{
	int start_print_pos = -1;

	assert(NULL != L);

	if (i < PRINT_NUM / 2)
		start_print_pos = 0;
	else if (i > (L->last - PRINT_NUM / 2))
		start_print_pos = L->last - PRINT_NUM;
	else
		start_print_pos = i - PRINT_NUM / 2;

	if (start_print_pos < 0)
		start_print_pos = 0;

	for (i = start_print_pos; i < (start_print_pos + PRINT_NUM) && (i <= L->last); i++)
		printf("%3d	", i);
	printf("\r\n");
	for (i = start_print_pos; i < (start_print_pos + PRINT_NUM) && (i <= L->last); i++)
		printf("%3d	", L->data[i]);
	printf("\r\n");

}

int main()
{
	int choice;
	int song_id = -1;
	int rst = -1;
	SeqList* L = NULL;

	//初始化歌曲列表资源
	L = initSeqList();
	if (NULL == L)
		return -1;

	while (1) {
		printf("---------operations---------\r\n");
		printf("0:退出\r\n");
		printf("1:添加歌曲\r\n");
		printf("2:删除歌曲\r\n");
		printf("3:查找歌曲\r\n");

		scanf("%d", &choice);
		switch (choice) {
		case 1:
			printf("请输入待添加歌曲曲号\r\n");
			scanf("%d", &song_id);
			rst = LocationSeqList(L, song_id);
			if (-1 != rst) {
				printf("待添加歌曲%d已存在\r\n", song_id);
				continue;
			}

			rst = insertSeqList(L, L->last + 2, song_id);
			if (0 == rst) {
				printf("歌曲%d添加成功\r\n", song_id);
				printList(L, L->last + 1);
			}
			else
				printf("歌曲%d添加失败\r\n", song_id);
			break;
		case 2:
			printf("请输入待删除歌曲曲号\r\n");
			scanf("%d", &song_id);
			int pos = LocationSeqList(L, song_id);
			if (-1 == pos) {
				printf("待删除歌曲%d不存在\r\n", song_id);
				continue;
			}

			rst = deleteSeqList(L, pos + 1);
			if (0 != rst)
				printf("歌曲%d删除失败\r\n", song_id);
			else {
				printf("歌曲%d删除成功\r\n", song_id);
				printList(L, pos);
			}

			break;
		case 3:
			printf("请输入待查找歌曲曲号\r\n");
			scanf("%d", &song_id);
			rst = LocationSeqList(L, song_id);
			if (-1 != rst) {
				printf("歌曲%d查找成功,在列表位置为%d\r\n", song_id, rst);
				printList(L, rst);
			}
			else
				printf("歌曲%d查找失败\r\n", song_id);
			break;
		case 0:
		default:
			printf("退出\r\n");
			destroySeqList(L);	//释放歌曲列表资源
			return 0;

		}
	}
	return 0;
}
#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//int main()		//牛客网新手训练125 —— 序列中去重数 —— 思路：定义两个（原/新）数组，进行比较，若不同则放在新数组中
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[1001] = { 0 }; 
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    int arr_copy[1001] = { 0 };
//    int m = 0;
//    for (int i = 0; i < n; i++)  //  重点
//    {
//        int j = 0;
//        for (j = 0; j < n; j++)
//        {
//            if (arr[i] == arr_copy[j])
//            {
//                break;
//            }
//        }
//        if (j == n)
//        {
//            arr_copy[m] = arr[i];
//            m++;
//        }
//    }
//
//    for (int i = 0; i < m; i++)
//    {
//        printf("%d ", arr_copy[i]);
//    }
//    return 0;
//}


#include <stdio.h>
#include<string.h>
int main()
{
    int n = 0;
    while (scanf("%d", &n) != EOF)
    {
        int arr1[20] = { 0 };
        int arr2[20] = { 0 };
        int m = 0;
        int num = 0;
        for (int i = 0; i < n-1; i++)
        {
            arr1[i] = i+2;
        }
        for (int i = 2; i < n; i++)
        {
            m = 0;
            for (int j = 0; j < n; j++)
            {
                
                if ((arr1[j] % arr1[i-2] == 0) && (arr1[j] != i))
                {
                     num++;
                }
                else
                {
                     arr2[m] = arr1[j];
                    m++;
                }
            }
            memcpy(arr1, arr2, sizeof(arr2));
            int k = 0;
            while (arr1[k] != 0)
            {
                arr2[k] = 0;
                k++;
            }
            n = k;
        }
        for (int i = 0; i < n; i++)
        {
            printf("%d ", arr1[i]);
        }
        printf("\n%d\n", num);
    }
    return 0;
}
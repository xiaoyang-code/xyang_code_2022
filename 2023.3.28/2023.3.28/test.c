#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
//int main()
//{
//	FILE* pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}

//int main()
//{
//	FILE* pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//
//	//写入一个字符
//	char c = 0;
//	for (c = 'a'; c <= 'z'; c++)
//	{
//		fputc(c, pf);
//	}
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

////往文件中写一个字符
//int main()
//{
//	FILE* pf = fopen("test.txt", "w");
//	if (pf != NULL)
//	{
//		char c = 0;
//		for (c = 'a'; c <= 'z'; c++)
//		{
//			fputc(c, pf);
//		}
//		fclose(pf);
//		pf = NULL;
//	}
// 
//	return 0;
//}

//从文件中读出一个字符串
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	char c = 0;
//	while (( c = fgetc(pf)) != EOF)
//	{
//		printf("%c ",c);
//	}
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (pf != NULL)
//	{
//		char c = 0;
//		while ((c = fgetc(pf)) != EOF)
//		{
//			printf("%c ", c);
//		}
//		fclose(pf);
//		pf = NULL;
//	}
//	else
//	{
//		printf("%s\n", strerror(errno));
//	}
//	return 0;
//}

//在文件中输入一行字符
//int main()
//{
//	FILE* pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	char c[20] = { 0 };
//	while ((gets(c) != NULL))
//	{
//		fputc('\n', pf);
//		fputs(c,pf);
//	}
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//int main()
//{
//	FILE* pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		printf("%s", strerror(errno));
//		return 1;
//	}
//	char c[20] = { 0 };
//	while (gets(c) != NULL)
//	{
//		fputs(c, pf);
//		fputc('\n', pf);
//	}
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//从文件中输出一行字符串
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	char c[100] = { 0 };
//	while (fgets(c, 100, pf) != NULL)
//	{
//		printf("%s", c);
//	}
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

int main()
{
	FILE* pf = fopen("test.txt", "r");
	if (pf == NULL)
	{
		perror("fopen");
		return 1;
	}
	char c[100] = { 0 };
	while (fgets(c, 100, pf) != NULL)
	{
		printf("%s", c);
	}
	fclose(pf);
	pf = NULL;
	return 0;
}
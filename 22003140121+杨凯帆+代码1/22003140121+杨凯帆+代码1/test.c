#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include<assert.h>
#include <stdio.h>
#include <malloc.h>

#define PRINT_NUM (10) //每次打印10个数据
#define ARRAY_SIZE (100)	//数组大小使用宏定义，数字使用括号括起
#define INVALID_FLAG (-1)	//无效标志
#define NULL ((void *)0) 

// 歌曲列表结构体定义
typedef struct tagSongList {
	int song_array[ARRAY_SIZE];	//数组存放歌曲号
	int song_numbers;	//歌曲总数
}SongList, * PSongList;

/*   I N I T   S O N G   L I S T   */
/*-------------------------------------------------------------------------
	Description: intiate song list
	Input : ppSongList : the sencond level pointer
	Result: void
-------------------------------------------------------------------------*/
void initSongList(PSongList* ppSongList)
{
	int i = 0;
	assert(NULL != ppSongList);

	*ppSongList = NULL;
	PSongList pSongList = (PSongList)malloc(sizeof(SongList));
	if (NULL == pSongList)
		return;

	//初始化歌曲数组，全部为无效
	for (i = 0; i < ARRAY_SIZE; i++)
		pSongList->song_array[i] = INVALID_FLAG;
	pSongList->song_numbers = 0;

	*ppSongList = pSongList;
	return;
}

/*   D E S T R O Y   S O N G   L I S T   */
/*-------------------------------------------------------------------------
	Description: destroy song list
	Input : pSongList : the pointer of the songlist
	Result: void
-------------------------------------------------------------------------*/
void destroySongList(PSongList pSongList)
{
	assert(NULL != pSongList);

	free(pSongList);
}

/*   S E A R C H   S O N G   */
/*-------------------------------------------------------------------------
	Description: search song
	Input: PSongList pSongList: the song list, int song_id: the id of the song
			to be found
	Result: the positon of the song to be found in the array
			-1: search fail
			others: search success
-------------------------------------------------------------------------*/
int searchSong(PSongList pSongList, int song_id)
{
	int i = 0;

	assert(NULL != pSongList);

	for (i = 0; i <= pSongList->song_numbers; i++) {
		if ( pSongList->song_array[i] == song_id )	//歌曲编号查找成功
			return i;	//返回歌曲在数组中的位置
	}
	return -1;
}

/*   A D D   S O N G   */
/*-------------------------------------------------------------------------
	Description: add song
	Input: PSongList pSongList: the song list, int song_id: the id of the song
			to be added
	Result: -1: add fail
			0: add success
-------------------------------------------------------------------------*/
int addSong(PSongList pSongList, int song_id)
{
	assert(NULL != pSongList);

	int pos = searchSong(pSongList, song_id);
	if (-1 != pos) {
		printf("歌曲%d已存在\r\n", song_id);
		return 0;
	}

	pos = searchSong(pSongList, INVALID_FLAG);
	if (-1 == pos)	//歌曲数组中没有空闲的位置
		return -1;	//添加失败 

	pSongList->song_array[pos] = song_id;	//在该空闲位置存储歌曲号
	pSongList->song_numbers++;	//歌曲总数+1

	return 0;	//添加成功
}

/*   D E L E T E  S O N G   */
/*-------------------------------------------------------------------------
	Description: delete song
	Input: PSongList pSongList: the song list, int song_id: the id of the song
			to be deleted
	Result: -1: delete fail
			0: delete success
-------------------------------------------------------------------------*/
int deleteSong(PSongList pSongList, int song_id)
{
	assert(NULL != pSongList);

	int pos = searchSong(pSongList, song_id);
	if (-1 == pos)	//歌曲数组中没有待删除歌曲
		return -1;	//删除失败

	pSongList->song_array[pos] = INVALID_FLAG;	//将歌曲所在位置元素置为无效标志
	(pSongList->song_numbers)--;	//歌曲总数-1

	return 0;	//删除成功
}

/*   P R I N T   L I S T   */
/*-------------------------------------------------------------------------
	Description: print song list , print 10 items in one time
	Input: PSongList pSongList: the song list, int song_id: the id of the song
	Result:
-------------------------------------------------------------------------*/
void printList(PSongList pSongList, int song_id)
{
	int start_print_pos = -1;
	int i = 0;

	assert(NULL != pSongList);

	int pos = searchSong(pSongList, song_id);
	if (-1 == pos)	//歌曲数组中没有待查找歌曲
		return;	//无需打印
	if (pos < PRINT_NUM / 2)
		start_print_pos = 0;
	else if (pos > ARRAY_SIZE - PRINT_NUM / 2)
		start_print_pos = ARRAY_SIZE - PRINT_NUM;
	else
		start_print_pos = pos - PRINT_NUM / 2;

	//每次只打印待查歌曲前后总共10个元素的信息
	for (i = start_print_pos; i < (start_print_pos + PRINT_NUM); i++)
		printf("%3d	", i);
	printf("\r\n");
	for (i = start_print_pos; i < (start_print_pos + PRINT_NUM); i++)
		printf("%3d	", pSongList->song_array[i]);
	printf("\r\n");

}

int main()
{
	int choice;
	int song_id = -1;
	int rst = -1;
	PSongList pSongList = NULL;

	//初始化歌曲列表资源
	initSongList(&pSongList);

	while (1) {
		printf("---------operations---------\r\n");
		printf("0:退出\r\n");
		printf("1:添加歌曲\r\n");
		printf("2:删除歌曲\r\n");
		printf("3:查找歌曲\r\n");

		scanf("%d", &choice);
		switch (choice) {
		case 1:
			printf("请输入待添加歌曲曲号\r\n");
			scanf("%d", &song_id);
			rst = addSong(pSongList, song_id);
			if (0 != rst)
				printf("歌曲%d添加失败\r\n", song_id);
			else
				printList(pSongList, song_id);
			break;
		case 2:
			printf("请输入待删除歌曲曲号\r\n");
			scanf("%d", &song_id);
			rst = deleteSong(pSongList, song_id);
			if (0 != rst)
				printf("歌曲%d删除失败\r\n", song_id);

			else
				printf("歌曲%d删除成功\r\n", song_id);
			break;
		case 3:
			printf("请输入待查找歌曲曲号\r\n");
			scanf("%d", &song_id);
			rst = searchSong(pSongList, song_id);
			if (-1 != rst) {
				printf("歌曲%d查找成功\r\n", song_id);
				printList(pSongList, song_id);
			}
			else
				printf("歌曲%d查找失败\r\n", song_id);
			break;
		case 0:
		default:
			destroySongList(pSongList);	//释放歌曲列表资源
			return 0;

		}
	}
}


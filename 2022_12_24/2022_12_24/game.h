#pragma once

//头文件的包含
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//符号的定义
#define ROW 3
#define COL 3

//函数的声明
// 
//棋盘的初始化
void Initboard(char board[ROW][COL], int row, int col);

//棋盘的打印
void DisplayBoard(char board[ROW][COL], int row, int col);

//玩家走
void PlayBoard(char board[ROW][COL], int row, int col);
//电脑走
void ComputerBoard(char board[ROW][COL], int row, int col);
//判断是否赢
char IsWin(char board[ROW][COL], int row, int col);









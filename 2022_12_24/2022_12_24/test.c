#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

void menu()
{
	printf("**********************************\n");
	printf("*******     1 . play      ********\n");
	printf("*******     2 . exit      ********\n");
	printf("**********************************\n");
}
void game()
{
	char ret = 0;
	char board[ROW][COL] ;
	//棋盘的初始化
	Initboard(board, ROW, COL);
	//棋盘的打印
	DisplayBoard(board, ROW, COL);

	while (1)
	{
		//玩家走
		PlayBoard(board, ROW, COL);
		DisplayBoard(board, ROW, COL);
		ret = IsWin(board, ROW, COL);
		if (ret != 'C')
			break;
		//电脑走
		ComputerBoard(board, ROW, COL);
		DisplayBoard(board, ROW, COL);
		ret = IsWin(board, ROW, COL);
		if (ret != 'C')
			break;
	}
	if (ret == '*')
		printf("玩家胜！！！\n");
	else if (ret == '#')
		printf("电脑胜！！！\n");
	else if(ret == 'Q')
		printf("平局！！！\n");
}
int main()
{
	int n = 0;
	srand((unsigned int)time(NULL));
	menu();
	do
	{
		printf("请输入:>");
		scanf("%d", &n);
		switch (n)
		{
		case 1:game();
			break; 
		case 0:
			break;
		default:printf("输入错误，请重新输入。\n");
			break;
		}
	} while (n);

	return 0;
}
#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include<stdio.h>

////A
//int main()
//{
//	int count = 0;
//	for (int i = 1; i <= 2020; i++)
//	{
//		int num = i;
//		while (num)
//		{
//			if ((num % 10) == 2)
//			{
//				count++;
//			}
//			num /= 10;
//		}
//	}
//	printf("%d \n", count);
//	return 0;
//}


////B
//int gad(int x, int y)
//{
//	if (x <= 0 || y <= 0)
//	{
//		return 0;
//	}
//	if (x % y == 0)
//	{
//		return y;
//	}
//	else
//	{
//		return gad(y, x % y);
//	}
//}
//int main()
//{
//	int count = 0;
//	for (int i = 1; i <= 2020; i++)
//	{
//		for (int j = 1; j <= 2020; j++)
//		{
//			if (gad(i, j) == 1)
//			{
//				count++;
//			}
//		}
//	}
//	printf("%d\n", count);
//	return 0;
//}

//int main()
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//	int ret = 0;
//	while (x%y!=0)
//	{
//		int tmp = x;
//		x = y;
//		y = tmp % y;
//	}
//	printf("%d ", y);
//	return 0;
//}

//C
//int main()
//{
//	int sum = 1;
//	for (int i = 0; i < 4; i++)
//	{
//		sum += i * 4;
//	}
//	printf("%d ", sum);
//	return 0;
//}

//回型矩阵

int main()
{
	int num = 1;
	int k = 0;//矩阵大小
	scanf("%d", &k);
	int arr[101][101] = { 0 };//使用二维数组 循环打印

	int l = 0;//左边界
	int r = k - 1;//右边界

	while (l < r)//外层循环 —— 控制回型路径
	{
		//内层有四个循环，
		//分别控制上行，右列，下行，左列，数值的填充

		for (int i = l; i < r; i++)//上行
		{
			arr[l][i] = num;
			num++;
		}
		for (int i = l; i < r; i++)//右列
		{
			arr[i][r] = num;
			num++;
		}
		for (int i = r; i > l; i--)//下行
		{
			arr[r][i] = num;
			num++;
		}
		for (int i = r; i > l; i--)//左列
		{
			arr[i][l] = num;
			num++;
		}

		//转完外圈，接着转内圈
		l++;
		r--;
	}
	if (k % 2 == 1)
	{
		arr[l][l] = num;
	}

	for (int i = 0; i < k; i++)
	{
		for (int j = 0; j < k; j++)
		{
			printf("%2d ", arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}


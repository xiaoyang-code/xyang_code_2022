#define _CRT_SECURE_NO_WARNINGS 1

//int main()
//{
//	int arr[10] = { 0 };
//	for (int i = 0; i < 10; i++)
//	{
//		scanf("%d ", &arr[i]);
//	}
//	int max = arr[0];
//	for (int i = 0; i < 10; i++)
//	{
//		if (max < arr[i])
//			max = arr[i];
//	}
//	printf("%d\n", max);
//	return 0;
//}


//int main()
//{
//	int n = 100;
//	float sum = 0;
//	for (int i = 1; i <= n; i++)
//	{
//		if (i % 2 == 0)
//		{
//			sum -= 1.0 / i;
//		}
//		else
//		{
//			sum += 1.0 / i;
//		}
//	}
//	printf("%.2f\n", sum);
//	return 0;
//}


//int main()
//{
//	int num = 0;
//	for (int i = 1; i <= 100; i++)
//	{
//		int t = i;
//		while (t)
//		{
//			if (t % 10 == 9)
//			{
//				num++;
//			}
//			t /= 10;
//		}
//	}
//	printf("%d\n", num);
//	return 0;
//}

//void menu()
//{
//	printf("********************************\n");
//	printf("*******  1.paly  0. exit   *****\n");
//	printf("********************************\n");
//}
//void game()
//{
//	printf("*****************  paly  ******************\n");
//	int a = 0;
//	int k = rand() % 100 + 1;
//	while (1)
//	{
//		printf("请输入你想猜的数字:>");
//		scanf("%d", &a);
//		if (a > k)
//		{
//			printf("猜大啦！\n");
//		}
//		else if (a < k)
//		{
//			printf("猜小啦！\n");
//		}
//		else
//		{
//			printf("猜对啦！就是：%d\n", k);
//			break;
//		}
//	}
//	printf("*****************  paly  ******************\n");
//}
//int main()
//{
//	int n = 0;
//	srand((unsigned)time(NULL));
//	do
//	{
//		menu();
//		printf("请选择:>");
//		scanf("%d", &n);
//		switch (n)
//		{
//		case 1:
//			printf("欢迎使用 1——100 之间的猜字游戏\n");
//			game();
//			break;
//		case 0:
//			printf("游戏结束！\n");
//			break;
//		default:
//			printf("输入错误，请重新输入。\n");
//			break;
//		}
//	} while (n);
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 0;
//	scanf("%d", &k);
//	int left = 0;
//	int right = sizeof(arr) / sizeof(arr[0]);
//	while (left <= right)
//	{
//		int mid = left + (right - left) / 2;
//		if (arr[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else if (arr[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			printf("找到啦，下标为：%d\n", mid);
//			break;
//		}
//	}
//	return 0;
//}

//牛客网新手训练124 —— 序列中删除指定数字
//#include <stdio.h>
//
//int main()   //  法一： 我的方法 —— 缺点 ：只能执行有序序列
//{
//    int n = 0;
//    int arr[51] = { 0 };
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    int a = 0;
//    scanf("%d", &a);
//    int flag = 0;  
//    for (int i = 0; i < n; i++)
//    {
//        if (arr[i] == a)
//        {
//            flag = 1;
//            for (int k = i; k < n - 1; k++)   
//            {
//                for (int j = k; j < n - 1; j++)
//                {
//                    arr[j] = arr[j + 1];
//                }
//            }
//            break;
//        }
//    }
//    for (int i = 0; i < n - flag; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//#include <stdio.h>
//
//int main()  //  法二：   规避“删除”二字， 找到删除项直接不打印
//                法三：   与法二大同小异 —— 主要思路为建立一个新数组，把删除项不存进新数组中，就实现了在数组中的“删除”
//{
//    int n = 0;
//    int arr[51] = { 0 };
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    int a = 0;
//    scanf("%d", &a);
//    for (int i = 0; i < n; i++)
//    {
//        if (arr[i] != a)
//        {
//            printf("%d ", arr[i]);
//        }
//    }
//    return 0;
//}

#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define Name_Max 20
#define Sex_Max 4
#define Tele_Max 13
#define Addr_Max 30

#define Inti_capacity 3
#define Add_capacity 2

typedef struct PeoInfo
{
	char Name[Name_Max];
	int SZ;
	char Sex[Sex_Max];
	char Tele[Tele_Max];
	char Addr[Addr_Max];

}PeoInfo;

typedef struct Contact
{
	PeoInfo* data;
	int sz;
	int capacity;

}Contact;


//初始化通信录
void IntiContact(Contact* pc);

//添加联系人
void AddContact(Contact* pc);

//销毁通讯录
void DestoryContact(Contact* pc);
#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)


#include "game.h"

void InitBoard(char board[ROWS][COLS], int rows, int cols, char set)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}

void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	printf("------扫雷游戏------\n");
	int i = 0;
	int j = 0;
	for (j = 0; j <= col; j++)
	{
		printf("%d ", j);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("------扫雷游戏------\n");
}

void SetMine(char board[ROWS][COLS], int row, int col)
{
	int count = EASY_COUNT;

	while (count)
	{
		int x = rand() % row + 1;
		int y = rand() % col + 1;
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (board[x][y] == '0')
			{
				board[x][y] = '1';
				count--;
			}
		}
	}
}

static int get_mine_count(char board[ROWS][COLS], int x, int y)
{
	int i = 0;
	int j = 0;
	int count = 0;
	for (i = -1; i <= 1; i++)
	{
		for (j = -1; j <= 1; j++)
		{
			if (board[x+i][y+j] == '1')
			{
				count++;
			}
		}
	}
	return count;
}

void OpenBoard(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y, int* count)
{
	int ret = get_mine_count(mine, x, y);
	if (ret == 0)
	{
		/*if (show[x][y] == '*')
		{	
			show[x][y] = ' ';
			(*count)++;
			int i = 0;
			int j = 0;
			for (i = -1; i <= 1; i++)
			{
				for (j = -1; j <= 1; j++)
				{
					OpenBoard(mine, show, x + i, y + j, &count);
				}
			}
		}*/

		show[x][y] = ' ';
		int i, j;
		for (i = -1; i <= 1; i++)
		{
			for (j = -1; j <= 1; j++)
			{
				if (show[x + i][y + j] == '*')
				{
					(*count)++;
					OpenBoard(mine, show, x + i, y + j, count);
				}
			}
		}
	}
	else
	{
		show[x][y] = ret + '0';
		(*count)++;
	}
}

//int FindBoard(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
//{
//	int x = 0;
//	int y = 0;
//	static int count = 0;
//	while(count < row * col - EASY_COUNT)
//	{
//		printf("请输入要排查的坐标:>");
//		scanf("%d %d", &x, &y);
//		if (x >= 1 && x <= row && y >= 1 && y <= col)
//		{
//			if (show[x][y] != '*')
//			{
//				printf("此坐标已被排查过，请重新输入\n");
//			}
//			else
//			{
//				if (mine[x][y] == '1')
//				{
//					printf("很遗憾，你被炸死了，游戏结束\n");
//					DisplayBoard(mine, ROW, COL);
//					break;
//				}
//				else
//				{
//					OpenBoard(mine, show, x, y, &count);
//					DisplayBoard(show, ROW, COL);
//					/*ret++;
//					int count = get_mine_count(mine, x, y);
//					show[x][y] = count + '0';
//					DisplayBoard(show, ROW, COL);*/
//				}
//			}
//		}
//		else
//		{
//			printf("坐标非法，请重新输入\n");
//		}
//	}
//	if (count == row * col - EASY_COUNT)
//	{
//		printf("恭喜你，挑战成功\n");
//		DisplayBoard(mine, ROW, COL);
//	}
//}

int FindBoard(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	static int count = 0;
	while (count < row * col - EASY_COUNT)
	{
		printf("请输入要排查的坐标:>");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			if (show[x][y] != '*')
			{
				printf("此坐标已被排查过，请重新输入\n");
			}
			else
			{
				if (mine[x][y] == '1')
				{
					printf("很遗憾，你被炸死了，游戏结束\n");
					DisplayBoard(mine, ROW, COL);
					return 0;
				}
				else
				{
					OpenBoard(mine, show, x, y, &count);
					DisplayBoard(show, ROW, COL);
					break;
					/*ret++;
					int count = get_mine_count(mine, x, y);
					show[x][y] = count + '0';
					DisplayBoard(show, ROW, COL);*/
				}
			}
		}
		else
		{
			printf("坐标非法，请重新输入\n");
		}
	}
	if (count == row * col - EASY_COUNT)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void SignBoard(char show[ROWS][COLS], int row, int col)
{
	int flag = 0;
	while (1)
	{
		printf("***********************\n");
		printf("1. 插旗\n");
		printf("2. 拔旗\n");
		printf("***********************\n");
		printf("请选择:>");
		scanf("%d", &flag);
		int x = 0;
		int y = 0;
		printf("请输入坐标:>");
		scanf("%d %d", &x, &y);
		if (flag == 1)
		{
			show[x][y] = '$';
			DisplayBoard(show, ROW, COL);
			break;
		}
		else if (flag == 2)
		{
			if (show[x][y] == '$')
			{
				show[x][y] = '*';
				DisplayBoard(show, ROW, COL);
				break;
			}
			else
			{
				printf("此处没旗帜，请重新输入坐标\n");
			}
		}
		else
		{
			printf("选择错误，请重新选择\n");
		}
	}

}
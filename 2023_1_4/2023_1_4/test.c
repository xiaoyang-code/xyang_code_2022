#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//////牛客网新手训练91 —— 水花数
////int main()
////{
////    int n, m;
////    int sum = 0;
////    int count = 0;
////    while (~scanf("%d %d", &m, &n))
////    {
////        for (int i = m; i <= n; i++)
////        {
////            int t = i;
////            sum = 0;
////            while (t)
////            {
////                sum = sum + (t % 10) * (t % 10) * (t % 10);
////                t = t / 10;
////            }
////            if (sum == i)
////            {
////                count++;
////                printf("%d ", i);
////            }
////        }
////        if (count == 0)
////        {
////            printf("no\n");
////        }
////        else
////        {
////            printf("\n");
////        }
////    }
////    return 0;
////}

//////////牛客网新手训练93
////////int main()
////////{
////////    int i = 0;
////////    int a[7] = { 0 };
////////    int max = 0;
////////    int min = 0;
////////    while (scanf("%d", &a[0]) != EOF)//在数组中多次输入，可以这样写
////////    {
////////        min = a[0];
////////        max = a[0];
////////        for (i = 0; i < 7; i++)
////////        {
////////            scanf("%d", &a[i]);
////////            if (max < a[i])
////////            {
////////                max = a[i];
////////            }
////////            if (min > a[i])
////////            {
////////                min = a[i];
////////            }
////////        }
////////        float sum = 0;
////////        for (i = 0; i < 7; i++)
////////        {
////////            sum = sum + a[i];
////////        }
////////        printf("%.2f\n", (sum - min - max) / 5.0);
////////    }
////////    return 0;
////////}

//牛客网新手训练95 —— 进制转换
////////////// 
// 
//法一：递归法——牛逼的很——递归就是倒序输出
//////// 
////////void fun(int a)
////////{
////////    if (a > 5)  
////////    {
////////        fun(a / 6);
////////    }
////////    printf("%d", a % 6);
////////}
////////int main()
////////{
////////    int a = 0;
////////    scanf("%d", &a);  
////////    fun(a);
////////    return 0;
////////}
//法二：短除法——使用数组
//int main()
//{
//	int a = 0;
//	int r = 0;
//	int i = 0;
//	int arr[10000] = { 0 };
//	scanf("%d", &a);
//	do
//	{
//		r = a % 6;
//		a /= 6;
//		arr[i] = r;
//		i++;
//
//	} while (a!=0);
//	for (int j = i - 1 ; j >= 0; j--)//因为do_while循环中最后一次要执行i++所以i比所需的i要多1（这就是j=i-1)的原因。
//	{
//		printf("%d", arr[j]);
//	}
//	return 0;
//}

//牛客网新手训练96 —— 金币
int main()
{
    int k = 0;
    int sum = 0;
    scanf("%d", &k);
    for (int i = 1; k > 0; i++)
    {
        k -= i;
        for (int j = 1; j <= i; j++)
        {
            sum += i;
        }

    }
    printf("%d\n", sum);
    return 0;
}
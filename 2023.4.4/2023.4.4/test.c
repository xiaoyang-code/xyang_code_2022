#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>
#include <stddef.h>
#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <string.h>


//typedef struct Stu
//{
//	char name;
//	int age;
//	char s1;
//	char s2;
//	int a1;
//	char s3;
//}Stu;
//#define OFFSETOF(type,name)  (size_t)(&(((type*)0)->name))
//int main() 
//{
//
//	printf("%d\n", (int)OFFSETOF(Stu, name));
//	printf("%d\n", (int)OFFSETOF(Stu, age));
//	printf("%d\n", (int)OFFSETOF(Stu, s1));
//	printf("%d\n", (int)OFFSETOF(Stu, s2));
//	printf("%d\n", (int)OFFSETOF(Stu, a1));
//	printf("%d\n", (int)OFFSETOF(Stu, s3));
//
//	return 0;
//}



//模拟实现atoi函数
//1. 空指针问题
//2. 空字符串问题
//3. 正负号问题
//4. 非数字字符问题
//5. 越界问题
//enum State
//{
//	LEGAL,
//	ILLEGAL
//};
//enum State state = ILLEGAL;
//int my_atoi(char* str)
//{
//	//空指针问题
//	assert(str != NULL);
//
//	//空字符串问题
//	if (*str == '\0')
//	{
//		return 0;
//	}
//
//	//符号问题
//	int flag = 1;
//	if (*str == '+')
//	{
//		flag = 1;
//		str++;
//	}
//	else if (*str == '-')
//	{
//		flag = -1;
//		str++;
//	}
//
//	//循环遍历
//	long long ret = 0; //越界问题
//	while (*str)
//	{
//		if (isdigit(*str))
//		{
//			ret = ret * 10 + (*str - '0');
//			if (ret > INT_MAX || ret < INT_MIN)
//			{
//				return (int)ret;
//			}
//		}
//		else
//		{
//			return (int)ret;
//		}
//		str++;
//	}
//	if (*str == '\0')
//	{
//		state = LEGAL;
//	}
//	return (int)ret;
//}
//int main()
//{
//	char arr[10] = { 0 };
//	scanf("%s", arr);
//	int ret = my_atoi(arr);
//	if (state == LEGAL)
//	{
//		printf("合法输出：%d\n", ret);
//	}
//	else
//	{
//		printf("非法输出：%d\n", ret);
//	}
//	return 0;
//}

//struct s
//{
//	char a1;
//	int a2;
//	char a3;
//};
//int main()
//{
//	struct s s1 = { 0 };
//	printf("%p\n", &(s1.a1));
//	printf("%p\n", &(s1.a2));
//	printf("%p\n", &(s1.a3));
//
//	int q = (int)&(((struct s*)0)->a2);
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	//00000000000000000000000000000011
//	//00000000000000000000000000001000
//	a |= (1 << 3);
//	//a = a | ( 1 << 3 ) ;
//	printf("%d\n", a);//1011
//
//	//00000000000000000000000000001011
//	//11111111111111111111111111110111
//	a &= (~(1 << 3));
//	printf("%d\n", a);
//
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	int b = 5;
//
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//
//	printf("A = %d ", a);
//	printf("B = %d ", b);
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int count = 0;
//	scanf("%d", &a);
//	while (a)
//	{
//		a = a & (a - 1);
//		count++;
//	}
//	printf("%d", count);
//	return 0;
//}

//int main()
//{
//	int arr[9] = { 1,1,2,3,4,3,4,5,5 };
//	int ret = 0;
//	int i = 0;
//	for (i = 0; i < 9; i++)
//	{
//		ret ^= arr[i];
//	}
//	printf("ret = %d", ret);
//	return 0;
//}

//int main()
//{
//	int a = -3;
//	unsigned int b = 2;
//	long c = a + b;
//	printf("%ld", c);
//	return 0;
//}

//int main()
//{
//    long long n = 0;
//    scanf("%lld", &n);
//    char chn[15] = { 0 };
//    sprintf(chn, "%lld", n);
//    int i = 0;
//    int num = 0;
//    while (n)  //位数
//    {
//        n /= 10;
//        num++;
//    }
//    char arr[15] = { 0 };
//    int count = 0;
//    int j = 0;
//    for (i = num - 1; i >= 0; i--, j++)
//    {
//        if (count % 3 == 0 && count != 0)
//        {
//            arr[j] = ',';
//            count = -1;
//            i++;
//        }
//        else
//        {
//            arr[j] = chn[i];
//        }
//        count++;
//    }
//    //反转字符串
//    for (i = 0, j = j-1; j > 0; j--, i++)
//    {
//        chn[j] = arr[i];
//    }
//    printf("%s\n", chn);
//    return 0;
//}

//char* fun(char* arr1, char* arr2, int sz, int num)
//{
//	char* start = arr1;
//	while (*arr2)
//	{
//		sz -= num;
//		int i = 0;
//		num = 0;
//		for (i = 0; i < sz; i++)
//		{
//			if ((*arr2) == (*(arr1 + i)))
//			{
//				num++;
//			    int j = 0;
//				for (j = i; j < sz - 1 - num; j++)
//				{
//					(*(arr1 + j)) = (*(arr1 + j + 1));
//				}
//
//			}
//		}
//		arr2++;
//	}
//	return start;
//}
//int main()
//{
//	char arr1[101] = { 0 };
//	char arr2[101] = { 0 };
//	scanf("%s", arr1);
//	scanf("%s", arr2);
//
//	int sz = strlen(arr1);//一共遍历多少次
//	int num = 0;//每次遍历删除了几个
//
//	char* ps = fun(arr1, arr2, sz, num);
//	printf("%s\n", ps);
//
//	return 0;
//}


//void print(int num, int sign) {
//	if (num <= 0) return;
//	print(num / 10, ++sign);
//	printf("%d", num % 10);
//	if (sign % 4 == 0) printf(",");
//	return;
//}

int main()
{
	char arr1[101] = { 0 };
	char arr2[101] = { 0 };

	gets(arr1);
	gets(arr2);

	int i = 0;
	while (arr2[i++])
	{
		if (strchr(arr2, arr1[i]) == NULL)
		{
			printf("%c", arr1[i]);
		}
	}
	return 0;
}
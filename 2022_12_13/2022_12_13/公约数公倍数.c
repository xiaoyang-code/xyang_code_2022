#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int main() {
//    //法一 —— 太过暴力，可能会不执行通过。
//    long long n, m;
//    scanf("%lld %lld", &n, &m);
//    long long max = n > m ? n : m;
//    while (1)
//    {
//        if (max % n == 0 && max % m == 0)
//            break;
//        max++;
//    }
//    long long min = n < m ? n : m;
//    while (1)
//    {
//        if (n % min == 0 && m % min == 0)
//            break;
//        min--;
//    }
//    printf("max = %lld min = %lld", max, min);
//
//    return 0;
//
//}

//int main()
//{
//	//法二 —— 辗转相除法
//	long long n, m;
//	scanf("%lld %lld", &n, &m);
//	long long a = n * m;
//	long long tmp = 0;
//	while (tmp = n % m)
//	{
//		n = m;
//		m = tmp;
//	}
//	long long max = m;
//	long long min = a / max;
//	printf("最小公倍数：%lld 最大公约数：%lld", max, min);
//	return 0;
//}
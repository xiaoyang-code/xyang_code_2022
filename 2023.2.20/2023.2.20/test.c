#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

//int main()
//{
//	int a = 10;
//	//对应二进制：00000000000000000000000000001010
//	//转化为16进制：00 00 00 0a
//	int b = -10;
//	//对应二进制：00000000000000000000000000001010
//	//      补码：11111111111111111111111111110110
//	//转化为16进制：ff ff ff f6
//	return 0;
//
//}


//int main()
//{
//	int a = 0x11223344;//  11 —  高字节处   44  — 低字节处
//	//       大端字节序存储  正序11223344  —— 把一个数据的低位字节的数据，存放在高地址处。把高位字节的数据，存放在低地址处。
//	//（vs）小端字节序存储  倒序44332211 —— 把一个数据的低位字节的数据，存放在低地址处。把高位字节的数据，存放在高地址处。
//	// 内存中 左边为低地址处，右边为高地址处
//	// 是字节的顺序 不是二进制的顺序
//	return 0;
//}
//int check_sys()
//{
//	int i = 1;
//	return (*(char*)&i);
//}
//int main()
//{
//	int ret = check_sys();
//	if (ret == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//	return 0;
//}
// 
// 
//int check_sys()
//{
//	union
//	{
//		int i;
//		char c;
//	}un;
//	un.i = 1;
//	return un.c;
//}




//int main()
//{
//	char a = -1;
//	signed char b = -1;
//	unsigned char c = -1;
//	printf("a=%d,b=%d,c=%d", a, b, c);
//	return 0;
//}
#include<stdio.h>
//int main()
//{
//	int i = -20;
//	10000000 00000000 00000000 00010100 - 原
//	11111111 11111111 11111111 11101011 - 反
//	11111111 11111111 11111111 11101100 - 补
//	unsigned char j = 10;
//	00000000 00000000 00000000 00001010 - 原反补
//	printf("%d\n", i + j);
//	11111111 11111111 11111111 11101100 - -20
//	00000000 00000000 00000000 00001010 - 10
//	11111111 11111111 11111111 11110110 - i+j补码
//	11111111 11111111 11111111 11110101 - 反码
//  10000000 00000000 00000000 00001010 - 原码  == -10
//	return 0;
//}


#include<windows.h>
//int main()
//{
//	unsigned int i;//4294967295
//	for (i = 9; i >= 0; i--)
//	{
//		printf("%u\n", i);
//		Sleep(1000);//单位是毫秒；
//	}
//	//此代码重点是 unsigned int 和 i>=0 这二点导致是一个死循环
//	//%u导致了0以后会打印出超大数 改成%d后就会出现负数，但依旧为死循环
//	return 0;
//}


//int main()
//{
//	char a[1000];
//	//char 类型的取值范围是 -128~127
//	int i;
//	for (i = 0; i < 1000; i++)
//	{
//		a[i] = -1 - i;
//	}
//  // 此循坏会执行1000 但因为是char，所以只能存储255个数（-1 -2 -3 .... -128 （此处符号的突变就是因为char类型和操作系统的截断导致的）127 126 125 ..... 3 2 1 0 -1 -2 -3....）
//	printf("%d", strlen(a));
//  //结果为255 ，因为strlen函数找到\0后就会结束。
//	return 0;
//}


//int main()
//{
//	unsigned char i = 0;
//	//0~255
//	for (i = 0; i <= 255; i++)//死循环，无符号数导致的
//	{
//		printf("hello world\n");
//	}
//	return 0;
//}


////////////////////////////////////////////////////////
//整型家族的类型的取值范围：limits.h
// 浮点型家族类型的取值范围：float.h
//浮点型
//int main()
//{
//	int n = 9;
//	float* pfloat = (float*)&n;
//	
//	printf("%d\n", n);  //9
//	printf("%f\n", *pfloat);   //9.000000
//
//	*pfloat = 9.0;
//	printf("%d\n", n);  //9
//	printf("%f\n", *pfloat);   //9.000000
//		
//	return 0;
//}

//以我们的分析得到得结果却只对了俩个，因为就是int类型和float类型得存储和解读得方式有别


//int main()
//{
//	float f = 5.5f;
//	//101.1
//	//(-1)^0 * 1.011 * 2^2
//	//0 10000001 01100000000000000000000  -  40b00000
//	return 0;
//}

////////////            指针                        ///////////

//int main()
//{
//	//字符指针
//	char ch = 'w';
//	char* pc = &ch;//pc就是字符指针
//	
//	const char* p = "abcdef";                     //表达式得值是 首字符地址（是把字符串首字符得地址存放在p中）
//	//const加上防止有些编译器报错                   //*p为常量字符串  不能修改
//
//	char arr[] = "abcdef";
//	char* pp = arr;//此时pp是可以修改的
//	return 0;
//}

//int main()
//{
//	char str1[] = "hello bit.";
//	char str2[] = "hello bit.";
//
//	const char* str3 = "hello bit.";
//	const char* str4 = "hello bit.";
//
//	if (str1 == str2)//他俩为数组，数组名为首元素的地址，因为他们的地址不同，所以为not same
//		printf("str1 and str2 are same\n");
//	else
//		printf("str1 and str2 are not same\n");
//
//	if (str3 == str4)//因为他们为常量字符串且存放的内容相同，所以他们指向同一块地址，所以他们为same
//		             
//		printf("str3 and str4 are same\n");
//	else
//		printf("str3 and str4 are not same\n");
//
//	return 0;
//}


//int main()
//{
//	//字符指针的数组
//	char* arr[] = { "abcdef","hehe","qwer" };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		printf("%s\n", arr[i]);
//	}
//
//	return 0;
//}

//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 2,3,4,5,6 };
//	int arr3[] = { 3,4,5,6,7 };
//	
//	//arr[i] == *(arr+i)
//	//arr是一个存放整型指针的数组
//	int* arr[] = { arr1,arr2,arr3 };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			//printf("%d ", arr[i][j]);
//			printf("%d ", *(arr[i] + j));
//
//		}
//		printf("\n");
//	}
//
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	printf("%p\n", arr);
//	printf("%p\n", &arr);
//	return 0;
//}
int main()
{
	int arr[10] = { 0 };
	printf("arr = %p\n", arr);
	printf("arr+1 = %p\n", arr + 1);

	printf("&arr= %p\n", &arr);
	printf("&arr+1= %p\n", &arr + 1);
	return 0;
}
#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

////模拟strlen函数
//#include <stdio.h>
//#include <assert.h>
//
//int my_strlen(const char* str)
//{
//	assert(*str != NULL);
//	int count = 0;
//	/*while (*str != '\0')
//	{
//		str++;
//		count++;
//	}*/
//	while (*str++)
//	{
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[10] = "0";
//	scanf("%s", &arr);
//
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}


////模拟strcpy函数
//#include <stdio.h>
//#include <assert.h>
//
//char* my_strcpy(char* dest, const char* source)
//{
//	assert(*dest && *source);
//	char* ret = dest;
//	while (*source != '\0')
//	{
//		*dest++ = *source++;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[10] = "qwerxxxx";
//	char arr2[10] = "rr  r";
//
//	char* p = my_strcpy(arr1, arr2);
//	printf("%s", p);
//	return 0;
//}


////模拟strcat函数
//#include <stdio.h>
//#include <assert.h>
//
//char* my_strcat(char* dest, const char* source)
//{
//	assert(*dest && *source);
//	char* ret = dest;
//	while (*dest)
//	{
//		dest++;
//	}
//	while (*dest++ = *source++)
//	{
//		;
//	}
//	return ret;
//
//}
//
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[20] = "world";
//
//	char* p = my_strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}


////模拟strcmp函数
//#include <stdio.h>
//#include <assert.h>
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(*str1 && *str2);
//	while (*str1++ == *str2++)
//	{
//		;
//	}
//	return (*str1 - *str2);
//}
//int main()
//{
//	char arr1[20] = "abccd";
//	char arr2[20] = "abcde";
//
//	int ret = my_strcmp(arr1, arr2);
//	if (ret > 0)
//	{
//		printf(">\n");
//	}
//	else if(ret == 0)
//	{
//		printf("=\n");
//	}
//	else
//	{
//		printf("<\n");
//	}
//	return 0;
//}


////模拟strstr函数
//#include <stdio.h>
//#include <assert.h>
//char* my_strstr(const char* str1, const char* str2)
//{
//	assert(*str1 && *str2);
//	char* s1 = NULL;
//	char* s2 = (char*)str2;
//	char* cp = (char*)str1;
//
//	while (*s2)
//	{
//		s1 = cp;
//		s2 = (char*)str2;
//		while (*s1 && *s2 && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//			if (*s2 == '\0')
//			{
//				return cp;
//			}
//		}
//		cp++;
//	}
//	return (NULL);
//}
//int main()
//{
//	char arr[20] = "this is a boy";
//	char* p = my_strstr(arr, "a");
//
//	printf("%s\n", p);
//	return 0;
//}

//笔试8
#include <stdio.h>
//int main()
//{
//	char* c[] = { "ENTER","NEW","POINT","FIRST" };
//	char** cp[] = { c + 3,c + 2,c + 1,c };
//	char*** cpp = cp;
//	printf("%s\n", **++cpp);
//	printf("%s\n", *-- * ++cpp + 3);
//	printf("%s\n", *cpp[-2] + 3);
//	printf("%s\n", cpp[-1][-1] + 1);
//	return 0;
//}

//
//int main()
//{
//	char* a[] = { "work","at","alibaba" };
//	char** pa = a;
//	pa++;
//	printf("%s\n", *pa);
//	return 0;
//}

//int main()
//{
//	int a[5] = { 1, 2, 3, 4, 5 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d,%d", *(a + 1), *(ptr - 1));
//	return 0;
//}

//struct Test
//{
//	int Num;
//	char* pcName;
//	short sDate;
//	char cha[2];
//	short sBa[4];
//}*p;
//int main()
//{
//	p = (struct Test*)0x00100000;//假设 p 的地址为此。
//	printf("%p\n", p + 0x1);
//	printf("%p\n", (unsigned long)p + 0x1);
//	printf("%p\n", (unsigned int*)p + 0x1);
//	return 0;
//}

//int main()
//{
//	int a[4] = { 1, 2, 3, 4 };
//	int* ptr1 = (int*)(&a + 1);
//	int* ptr2 = (int*)((int)a + 1);
//	printf("%x,%x", ptr1[-1], *ptr2);
//	return 0;
//}

//int main()
//{
//	int a[3][2] = { (0, 1), (2, 3), (4, 5) };
//	int* p;
//	p = a[0];
//	printf("%d", p[0]);
//	return 0;
//}


//int main()
//{
//	int aa[2][5] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}


//int main()
//{
//	int a[5][5];
//	int(*p)[4];
//	p = a;
//	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);
//	return 0;
//}


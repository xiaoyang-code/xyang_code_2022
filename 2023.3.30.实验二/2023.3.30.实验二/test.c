#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)
/*-------------------------------------------------------------------------
	Description: queue operations
	Author: liuxin_dz
	Date: 2023-03-21
-------------------------------------------------------------------------*/
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define MAXSIZE (200)   /*根据实际需要设置队列的最大容量*/ 

typedef int datatype;
typedef struct
{
	datatype data[MAXSIZE];      /*数据的存储区*/
	int front, rear;         /*队头队尾指针*/
	int num;                /*队中元素的个数*/
}CSeQueue;          /*循环队*/


/*   I N I T _   S E   Q U E U E   */
/*-------------------------------------------------------------------------
	Description: 构建一个空的循环队算法
	Result: 返回创建的循环队列的指针
-------------------------------------------------------------------------*/
CSeQueue* Init_SeQueue()
{
	CSeQueue* q = NULL;
	q = (CSeQueue*)malloc(sizeof(CSeQueue));
	if (q == NULL)
	{
		return NULL;
	}
	q->front = MAXSIZE - 1;
	q->rear = MAXSIZE - 1;
	q->num = 0;
	return q;
}

/*   D E S T R O Y _   S E   Q U E U E   */
/*-------------------------------------------------------------------------
	Description: 销毁循环队算法
	Result: void
-------------------------------------------------------------------------*/
void Destroy_SeQueue(CSeQueue* q)
{
	assert(NULL != q);

	free(q);
	q = NULL;
}


/*   I N _   S E   Q U E U E   */
/*-------------------------------------------------------------------------
	Description: 循环队列入队操作
	Input: CSeQueue *q,datatype x
	Result: 0： success
			-1：fail
-------------------------------------------------------------------------*/
int In_SeQueue(CSeQueue* q, datatype x)
{
	if (q->num == MAXSIZE)
	{
		printf("队满\r\n");
		return -1;        /*队满不能入队*/
	}
	else
	{
		q->rear = (q->rear+1)%MAXSIZE;
	    //队尾指针向前移动一个
		q->data[q->rear] = x;
		q->num++;
		return 0;    	/*入队完成*/
	}
}

/*   O U T _   S E   Q U E U E   */
/*-------------------------------------------------------------------------
	Description: 循环队列出队操作
	Input: CSeQueue *q,datatype *x
	Result: 0： success
			-1：fail
-------------------------------------------------------------------------*/
int Out_SeQueue(CSeQueue* q, datatype* x)
{
	if (q->num == 0)
	{
		printf("队空\r\n");
		return -1;   	             /*队空不能出队*/
	}
	else
	{
		q->front =(q->front+1)% MAXSIZE;
	//队头指针向上移动一个
		*x = q->data[q->front]; 	/*读出队头元素*/
		q->num--;
		return 0;                           /*出队完成*/
	}
}


int main()
{
	CSeQueue* CS;
	int choice;
	datatype x;
	int ret = 0;

	//初始化循环队列
	CS = Init_SeQueue();

	while (1)
	{
		printf("---------operations---------\r\n");
		printf("0:退出\r\n");
		printf("1:写数据到缓冲区\r\n");
		printf("2:从缓冲区读数据\r\n");

		scanf("%d", &choice);
		switch (choice) {
		case 1:
			printf("请输入写入数据\r\n");
			scanf("%d", &x);
			In_SeQueue(CS, x);
			break;
		case 2:
			ret = Out_SeQueue(CS, &x);
			if (0 == ret)
				printf("读出数据为 %d\r\n", x);
			break;

		case 0:
		default:
			Destroy_SeQueue(CS);
			break;
		}

	}
	return 0;
}
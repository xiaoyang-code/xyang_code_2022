#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

void reverse(char* str, int num)
{
    int l = 0;
    int r = num - 1;
    while (l < r)
    {
        char tmp = *(str + l);
        *(str + l) = *(str + r);
        *(str + r) = tmp;
        l++;
        r--;
    }
}
char* baseNeg2(int n) {
    char arr[32] = { 0 };
    int count = 0;
    while (n != 0)
    {
        arr[count] = n % (-2);
        n /= (-2);
        count++;
    }
    reverse(arr, count);
    return arr;
}
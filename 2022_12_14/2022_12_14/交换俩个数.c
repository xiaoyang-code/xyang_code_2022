#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int main()
//{
//	//法一 —— 创造了一个新的临时变量
//	int a = 10;
//	int b = 20;
//	printf("a=%d b=%d\n", a, b);
//	int tmp = 0;
//	tmp = a;
//	a = b;
//	b = tmp;
//	printf("a=%d b=%d\n", a, b);
//	return 0;
//}

//  不创建新的临时变量

//int main()
//{
//	//法二 —— 加减法 —— 缺点：可能出现溢出情况
//	int a = 10;
//	int b = 20;
//	printf("a=%d b=%d\n", a, b);
//	a = a + b;
//	b = a - b;
//	a = a - b;
//	printf("a=%d b=%d\n", a, b);
//  return 0;
//}

//int main()
//{
//	//法三 —— 通过与或操作符 将其转换为2进制位 —— 优点：不存在溢出情况 —— 缺点：执行效率不如法一，可读性不如法一。
//	int a = 10;
//	int b = 20;
//	printf("a=%d b=%d\n", a, b);
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("a=%d b=%d\n", a, b);
//	return 0;
//}#include <stdio.h>
#include<math.h>
int main()
{
    float a = 0;
    char b = 0;
    scanf("%f %c", a, b);
    int m = 20;
    if (b == 'y')
    {
        m += 5;
        if (a > 1)
        {
            m += ceil(a - 1);
        }
    }
    else if (b == 'n')
    {
        if (a > 1)
        {
            m += ceil(a - 1);
        }
    }
    printf("%d", m);
    return 0;
}
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int main()
//{
//	//法一 —— 短除法求二进制 —— 缺点：不能执行负数
//	int a;
//	scanf("%d", &a);
//	int num = 0;
//	while (a)
//	{
//		if (a % 2 == 1)
//			num++;
//		a /= 2;
//
//	}
//	printf("%d", num);
//
//	return 0;
//}

//int main()
//{
//	//法二 —— 使用与或操作符和移位操作符 —— 缺点：效率不高
//	int a;
//	scanf("%d", &a);
//	int num = 0;
//	for (int i = 0; i < 32; i++)
//	{
//		if ((a >> i) & 1 == 1)
//			num++;
//	}
//	printf("%d", num);
//
//	return 0;
//}

//int main()
//{
//	//法三 —— 法二的优化进阶版
//	int a;
//	scanf("%d", &a);
//	int num = 0;
//	while (a)
//	{
//		a &= (a - 1);
//		num++;
//	}
//	printf("%d", num);
//
//	return 0;
//}
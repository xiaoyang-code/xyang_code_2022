#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)


//作业
// 
// 交换两个变量（不创建临时变量）
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int b = 20;
//	printf("before: a = %d, b = %d\n", a, b);
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("after: a = %d, b = %d\n", a, b);
//
//	return 0;
//}

//统计二进制中1的个数
//#include<stdio.h>
//int count(int n)
//{
//	int ret = 0;
//	for (int i = 0; i < 32; i++)
//	{
//		if (1 == (n & 1))
//		{
//			ret++;
//		}
//		n = n >> 1;
//	}
//	return ret;
//}
//int main()
//{
//	int a = 0;
//	scanf("%d ", &a);
//	printf("%d\n" , count(a));
//
//	return 0;
//}



//#include<stdio.h>
//int main()
//{
//	//二进制的学习
//	//正整数的原返补相同
//	//负整数的原返补需要计算
//	//反码：符号位不变，其他位按位取反
//	//补码：反码+1 （内存中都以补码的形式储存）
//	int a = 5;
//	//0000000000000000000000000000101 - 原码
//	//0000000000000000000000000000101 - 反码
//	//0000000000000000000000000000101 - 补码
//
//	int b = -5;
//	//1000000000000000000000000000101 - 原码
//	//1111111111111111111111111111010 - 反码
//	//1111111111111111111111111111011 - 补码
//
//	//位操作符（&，|,^)
//	// & 按位与 有0为0，无0为1
//	// | 按位或 有1为1，无1为0
//	// ^ 按位异或 相同为1，不同为0
//	
//	//按位异或的公式
//	// a ^ a = 0
//	// 0 ^ a = a
//	// 
//	// 3^3^5 = 5
//	// 3^5^3 = 5
//	//按位异或是有交换律的
//	return 0;
//}
// 

//int test1(int arr[])
//{
//	return sizeof(arr);
//}
//int test2(char ch[])
//{
//	return sizeof(ch);
//}
//
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	char ch[10] = { 0 };
//	test1(arr);
//	test2(ch);
//	printf("arr = %d, ch = %d\n", sizeof(arr), sizeof(ch));
//
//	printf("arr = %d, ch = %d", test1(arr), test2(ch));
//	return 0;
//}
//


//#include<stdio.h>
//int main()
//{
//	int a = 0, b = 2, c = 3, d = 4;
//	int i = 0;
//
//	i = a++ && ++b && d++;
//	printf("a = %d, b = %d, c = %d, d = %d\n", a, b, c, d);
//	printf("i = %d\n", i);
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	int odd[15] = { 0 };
//	int even[15] = { 0 };
//
//	int o = n;
//	for (int i = 0; i < 15; i++)
//	{
//		odd[i] = (o & 1);
//		o = o >> 2;
//	}
//	int e = n;
//	for (int j = 0; j < 15; j++)
//	{
//		if (0 == j)
//		{
//			e = e >> 1;
//		}
//		even[j] = (e & 1);
//		e = e >> 2;
//	}
//
//	for (int i = 0; i < 15; i++)
//	{
//		printf("%d ", odd[i]);
//	}
//	printf("\n");
//	for (int j = 0; j < 15; j++)
//	{
//		printf("%d ", even[j]);
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	int m = 0;
//	int count = 0;
//	scanf("%d %d", &n, &m);
//	
//	
//	for (int i = 0; i < 32; i++)
//	{
//		if (((n >> i) & 1) != ((m >> i) & 1))
//		{
//			count++;
//		}
//	}
//
//	printf("%d\n", count);
//	return 0;
//}

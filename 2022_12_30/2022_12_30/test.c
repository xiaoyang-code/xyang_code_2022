#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"

void menu()
{
	printf("************************************\n");
	printf("*********    1 . play       ********\n");
	printf("*********    0 . exit       ********\n");
	printf("************************************\n");
}
void game()
{
	//存放地雷的棋盘
	char Mine[ROWS][COLS] = { 0 };
	//展示给用户看的棋盘
	char Show[ROWS][COLS] = { 0 };
	//初始化棋盘
	InitBoard(Mine, ROWS, COLS, '0');
	InitBoard(Show, ROWS, COLS, '*');
	//布置地雷
	SetMine(Mine, ROW, COL);
	system("cls");//清空屏幕
	//打印棋盘
	DisplayBoard(Mine, ROW, COL);
	DisplayBoard(Show, ROW, COL);
	//开始扫雷
	FineMine(Mine, Show, ROW, COL);
}
int main()
{
	srand((unsigned int)time(NULL));
	int input = 0;
	do
	{
		menu();
		printf("请输入:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("游戏结束\n");
			break;
		default:
			printf("输入错误，请重新输入\n");
			break;
		}
	} while (input);
	return 0;
}



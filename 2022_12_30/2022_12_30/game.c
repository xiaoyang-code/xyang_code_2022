#define _CRT_SECURE_NO_WARNINGS 1


#include"game.h"

//初始化棋盘
void InitBoard(char board[ROWS][COLS], int row, int col, char set)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			board[i][j] = set;
		}
	}
}
//打印棋盘
void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	printf("----------扫雷游戏----------\n");
	for (i = 0; i <= row; i++)
	{
		printf(" %d ", i);
		if (i < row)
		{
			printf("|");
		}
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		for (j = 1; j <= col+1; j++)
		{
			printf("----");
		}
		printf("\n");
		printf(" %d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("|");
			printf(" %c ", board[i][j]);
		}
		printf("\n");
	}
	printf("----------扫雷游戏----------\n");
}
//布置地雷
void SetMine(char board[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int count = COUNT;
	while (count)
	{
		x = rand() % row + 1;
		y = rand() % row + 1;

		if (board[x][y] == '0')
		{
			board[x][y] = '1';
			count--;
		}
	}
}
//记录周围地雷的个数
int get_mine_count(char mine[ROWS][COLS], int x, int y)
{
	int i = 0;
	int j = 0;
	int count = 0;

	for (i = -1; i <= 1; i++)
	{
		for (j = -1; j <= 1; j++)
		{
			if (i == '0' && j == '0')
			{
				continue;
			}
			else if (mine[x+i][y+j] == '1')
			{
				count++;
			}
		}
	}
	return count;
}
//炸金花展开函数
void explode_spread(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col, int x, int y)
{
	//防止非法坐标展开（防止越界）
	if (x >= 1 && x <= row && y >= 1 && y <= col)
	{
		int count = get_mine_count(mine, x, y);
		if (count == 0)//判断周围是否有地雷
		{
			show[x][y] = ' ';
			int i = 0;
			int j = 0;
			for (i = -1; i <= 1; i++)
			{
				for (j = -1; j <= 1; j++)
				{
					if (show[x + i][y + j] == '*')
					{
						explode_spread(mine, show, row, col, x, y);
					}
				}
			}
		}
		else
		{
			show[x][y] = count + '0';
		}
	}
}
//扫雷函数
void FineMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int count = 0;
	int win = 0;
	while (win<row * col - COUNT)
	{
		printf("请输入你选择的坐标:>");
		scanf("%d %d", &x, &y);
		if (mine[x][y] == '1')
		{
			break;
		}
		else
		{
			explode_spread(mine, show, row, col, x, y);
			DisplayBoard(show, ROW, COL);
		}
	}

}
#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>


//有趣的代码
//
////代码1
//( * ( void (*)() ) 0 )();
////
////这是一段很有趣的代码，当首次看到这段代码的时候，我们都会脑袋一蒙，
////不知道该如何去思考，
////其实，我们就只能从我们所熟悉的东西去思考啊，首先看得懂的只有 0 与 void 
//// 而 0 前面是一个括号， 括号里写着 void (*)() 这是一个函数指针啊，
////所以就是将 int 类型的 0 强制类型转化成 函数指针，然后再 * 解引用，
////找到这个函数，最后的（）是调用这个函数。
//
////代码2
//void (*signal (int, void(*)(int) ) )(int);
//
//// 这个比上一个好点，就是因为有个函数名，所以我们可以从函数名开始分析
//// （）的优先级高于 * 所以 signal 会先和( int, void(*)(int) ) 结合为一个函数
//// 此时已经有了函数名和函数的参数，还差什么呢？当然是函数的类型了，
//// 所以将 signal (int, void(*)(int) ) 去掉 就是signal函数的类型了为 void (*)(int)
//// 所以上述代码是一个函数的声明
//// 这个函数的函数名为 signal
//// signal 的第一个参数是 int 类型，第二个参数是 void(*)(int) 类型的函数指针
//// 该函数指针 的参数是 int 类型 返回值是 void 类型
//// signal 函数的返回类型也是一个函数指针
//// 该函数的参数为 int 类型，返回值为 void 类型
//


//void menu()
//{
//	printf("********************************\n");
//	printf("****** 1. Add      2. Sub  *****\n");
//	printf("****** 3. Mul      4. Div  *****\n");
//	printf("****** 0. exit             *****\n");
//	printf("********************************\n");
//}
//int Add(int x, int y)
//{
//	return x + y;
//}
//int Sub(int x, int y)
//{
//	return x - y;
//}
//int Mul(int x, int y)
//{
//	return x * y;
//}
//int Div(int x, int y)
//{
//	return x / y;
//}
//void Cale(int (*pf)(int ,int))  //  此函数为 回调函数（在一个函数中调用所传过来的是函数指针的形参变量）目的是，避免switch - case 语句中冗余的现象
//{
//	int x = 0;
//	int y = 0;
//	printf("请输入俩个操作数:>");
//	scanf("%d %d", &x, &y);
//	int ret = pf(x, y);
//	printf("%d\n", ret);
//	return 0;
//}
//int main()
//{
//	int input = 0;
//	
//	do
//	{
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Cale(Add);
//			break;
//		case 2:
//			Cale(Sub);
//			break;
//		case 3:
//			Cale(Mul);
//			break;
//		case 4:
//			Cale(Div);
//			break;
//		case 0:
//			printf("退出计算器\n");
//			break;
//		default:
//			printf("输入错误，请重新选择\n");
//		}
//	} while (input);
//
//	return 0;
//}


//   qsort函数的学习


//void qsort(void* base, // 指向了待排序数组的第一个元素
//	size_t num,//待排序数组的元素个数
//	size_t size,//每一个元素的大小，单位是字节
//	int (*cmp)(const void*, const void*)// 指向一个函数，这个函数可以比较2个元素的大小
//	);
#include <stdlib.h>
#include <string.h>

//qsort函数的使用者提供这个函数
int cmp_int(const void* p1, const void* p2)
{
	return *(int*)p1 - *(int*)p2;
}

void print_arr(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

test1()
{
	int arr[] = { 3,1,5,2,4,9,8,6,5,7 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//使用qsort来排序整型数组，这里就要提供一个比较函数，这个比较函数能够比较2个整数的大小
	//qsort 默认是排成升序的
	qsort(arr, sz, sizeof(arr[0]), cmp_int);
	print_arr(arr, sz);
}

//测试qsort 排序结构体数据
struct Stu
{
	char name[20];
	int age;
};

//按照年龄来比较
int cmp_stu_by_age(const void* p1, const void* p2)
{
	return ((struct Stu*)p1)->age - ((struct Stu*)p2)->age;
}

int cmp_stu_by_name(const void* p1, const void* p2)
{
	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
}

void test2()
{
	struct Stu s[] = { {"zhangsan", 30}, {"lisi", 25}, {"wangwu", 50} };
	int sz = sizeof(s) / sizeof(s[0]);
	//测试按照年龄来排序
	//qsort(s, sz, sizeof(s[0]), cmp_stu_by_age);
	//测试按照名字来排序
	qsort(s, sz, sizeof(s[0]), cmp_stu_by_name);
}

int main()
{
	test1();
	test2();

	return 0;
}
















#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//int main()
//{
//	int a[] = { 1,2,3,4,5,6,7,8,9,0 };
//	int b[] = { 0,9,8,7,6,5,4,3,2,1 };
//	int sz = sizeof(a) / sizeof(a[0]);
//	int i = 0;
//	while (sz--)
//	{
//		int tmp = 0;
//		tmp = a[i];
//		a[i] = b[i];
//		b[i] = tmp;
//		i++;
//	}
//	for ( i = 0; i < 10; i++)
//	{
//		printf("%d", a[i]);
//	}
//	return 0;
//}

//void init(int a[],int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		a[i] = 0;
//	}
//}
//
//void print(int a[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", a[i]);
//	}
//}
//
//void reverse(int a[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left < right)
//	{
//		int tmp = 0;
//		tmp = a[left];
//		a[left] = a[right];
//		a[right] = tmp;
//		left++;
//		right--;
//	}
//}

int main()
{
	int arr[] = { 1,2,3,6,5,4,8,7,9 };
	int sz = sizeof(arr) / sizeof(arr[0]);

	for (int j = 0; j < sz - 1; j++)
	{
		for (int i = 0; i < sz - j - 1; i++)
		{
			if (arr[i] > arr[i + 1])
			{
				int tmp = 0;
				tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
			}
		}
	}
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}
#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

/*-------------------------------------------------------------------------
  Description: 	search:
				SeqSearch
				binarySearch
				sort:
				InsertSort
				BubbleSort
				QuickSort
				SelectSort
  Author: liuxin_dz
  date:2023/05/12
-------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>

typedef int datatype;

#define STATISTIC 1

//性能统计时需要关闭打印函数
#if STATISTIC
/*   P R I N T   A R R A Y   */
/*-------------------------------------------------------------------------
	Descriptor: print array, from start to end
	IN: int array[], int end, int start
	Ret: void
-------------------------------------------------------------------------*/
void inline PrintArray(int array[], int end, int start)
{}

#define Print(...)  ((void)0)

#else
/*   P R I N T   A R R A Y   */
/*-------------------------------------------------------------------------
	Descriptor: print array, from start to end
	IN: int array[], int end, int start
	Ret: void
-------------------------------------------------------------------------*/
void inline PrintArray(int array[], int end, int start)
{
	int i;

	if (start < 0)
		start = 0;

	for (i = start; i < end; i++)
	{
		printf("%d ", array[i]);
	}
	printf("\r\n");
}
#define Print(...) printf(__VA_ARGS__)
#endif

/*--------------------------------------------------------------*/
/*	交换x,y
/*--------------------------------------------------------------*/
void inline swap(int* x, int* y)
{
	int temp;
	temp = *x;
	*x = *y;
	*y = temp;
}

/*  E N S E Q U E N C E  S E A R C H   */
/*-------------------------------------------------------------------------
	Descriptor: envolved sequence search
	IN: datatype dataArray[], datatype key, int arrayLength
	Ret: int: the index of key
-------------------------------------------------------------------------*/
int EnSeqSearch(datatype dataArray[], datatype key, int arrayLength)
{
	int i = arrayLength;
	assert(NULL != dataArray);
	dataArray[0] = key;

	while (dataArray[i] != key)		//当前位置元素和key进行比较，不相等继续查找 
		i--;
	if (0 == i)
		Print("顺序查找失败\r\n");
	else
		Print("顺序查找成功，位置索引：%d\r\n", i);

	return i;	//返回查找元素所在的位置索引，查找失败时返回0 
}

/*   S E Q U E N C E  S E A R C H   */
/*-------------------------------------------------------------------------
	Descriptor: sequence search
	IN: datatype dataArray[], datatype key, int arrayLength
	Ret: int: the index of key
-------------------------------------------------------------------------*/
int SeqSearch(datatype dataArray[], datatype key, int arrayLength)
{
	int i = 0;
	assert(NULL != dataArray);

	while (i < arrayLength
		&& dataArray[i] != key)		//当前位置元素和key进行比较，不相等继续查找 
		i++;
	if (i == arrayLength)
		Print("顺序查找失败\r\n");
	else
		Print("顺序查找成功，位置索引：%d\r\n", i + 1);

	return (i == arrayLength) ? 0 : (i + 1);	//返回查找元素所在的位置索引，查找失败时返回0 
}

/*   B I N A R Y  S E A R C H   */
/*-------------------------------------------------------------------------
	Descriptor: binary search
	IN: datatype dataArray[], datatype key, int arrayLength
	Ret: int: the index of key
-------------------------------------------------------------------------*/
int binarySearch(datatype dataArray[], datatype key, int arrayLength)
{
	int mid;
	int low = 0;

	assert(arrayLength > 0);
	int high = arrayLength - 1;

	while (low <= high)
	{
		mid = (low + high) / 2;
		if (key == dataArray[mid])
		{
			Print("折半查找成功，位置索引：%d\r\n", mid + 1);
			return mid + 1;
		}
		else if (key < dataArray[mid])
			high = mid - 1;
		else
			low = mid + 1;
	}
	Print("折半查找失败\r\n");
	return 0;
}

/*   I N S E R T   S O R T   */
/*-------------------------------------------------------------------------
	Descriptor: insert sort
	IN: int array_length
	INOUT: int array[]
	Ret: void
-------------------------------------------------------------------------*/
void InsertSort(int array[], int array_length)
{
	int i, j;
	int insert_value;                	/*待插入的数值*/

	for (i = 1; i < array_length; i++)    /*依序插入数值array[i]*/
	{
		insert_value = array[i];     /*用insert_value保存待插入的array[i]*/
		j = i - 1;                       /*从后向前进行比较，比较的范围为array[i-1]到array[0]*/
		while (j >= 0 && insert_value < array[j])
			/*当array[j]<insert_value, 将array[j]后移到array[j+1]*/
		{
			array[j + 1] = array[j];
			j--;
		}
		array[j + 1] = insert_value;   /*将数值插入*/

		/************打印目前排序结果***************/
		Print("\n After [%d] insert sorting result:", i);
		PrintArray(array, array_length, 0);
	}
}

/*   B U B B L E   S O R T   */
/*-------------------------------------------------------------------------
	Descriptor: bubble sort
	IN: int array_length array_length为元素个数
	INOUT: int array[]，array为待排序的数组
	Ret: void
-------------------------------------------------------------------------*/
void BubbleSort(int array[], int array_length)
{
	int i, j;
	int change_flag;     /*记录某趟排序数值是否有交换位置*/
	int temp;           /*数值交换时的暂存变量*/

	assert(NULL != array);
	assert(array_length > 0);

	for (j = array_length - 1; j > 0; j--)   /*外层循环，控制趟数，共进行n-1趟*/
	{
		change_flag = 0;
		for (i = 0; i < j; i++)  /*内层循环，控制每一趟内部的比较次数*/
		{
			if (array[i] > array[i + 1])
			{
				/*如果前者大于后者，两个数值交换位置,有数值交换时，change_flag置1*/
				swap(&array[i], &array[i + 1]);
				change_flag = 1;
			}
		}

		/*------打印目前的排序结果------*/
		Print("\n After [%d] bubble sorting result:", array_length - j);
		PrintArray(array, array_length, 0);

		if (!change_flag)
			break; /*当在一趟的比较中没有任何数据交换，则结束排序*/
	}
}

/*   Q U I C K   S O R T   */
/*-------------------------------------------------------------------------
	Descriptor: quick sort
	IN: int low, int high, int array_length
	INOUT: int array[]
	Ret: void
-------------------------------------------------------------------------*/
void QuickSort(int array[], int low, int high)
{
	int i, j; /*i，j分别为左右下标值*/
	int pivot;  /*分割值*/

	assert(NULL != array);

	if (low < high) /*递归结束条件为low>=high*/
	{
		Print("\nbefore QuickSort is:\n");
		PrintArray(array, high + 1, low);

		i = low;
		j = high;

		pivot = array[low]; /*以待排序数组最左元素值为分割值*/

		while (i < j)
		{
			/*从右往左查找第一个比分割值小的元素，将它的值赋值给左下标对应的元素*/
			while (i < j && array[j] < pivot)j--;
			/*array[i] = array[j];*/
			if (i < j)
				swap(&array[i], &array[j]);
			/*从左往右查找第一个比分割值大的元素，将它的值赋值给右下标对应的元素*/
			while (i < j && array[i] > pivot)i++;
			/*array[j] = array[i];*/
			if (i < j)
				swap(&array[i], &array[j]);
		}
		/*当i和j相等时，将分割值赋值给i或者j下标对应的元素，即新的分割元素*/
		array[i] = pivot;

		Print("the partion value array[%d] is:%d", i, array[i]);

		Print("\nafter QuickSort is:\n");
		PrintArray(array, high + 1, low);

		/*分割元素以左的数组元素重新快速排序*/
		QuickSort(array, low, i - 1);
		/*分割元素以右的数组元素重新快速排序*/
		QuickSort(array, i + 1 , high);
	}
}

/*   S E L E C T   S O R T   */
/*-------------------------------------------------------------------------
	Descriptor: select sort
	IN: int array_length
	INOUT: int array[]
	Ret: void
-------------------------------------------------------------------------*/
void SelectSort(int array[], int array_length)
{
	int i, j;
	int min_value;   /*存储最小数值*/
	int min_index;  /*存储最小数值的索引值*/
	int temp;      /*数值交换时的暂存变量*/

	assert(NULL != array);
	assert(array_length > 0);

	for (i = 0; i < array_length - 1; i++)    /*外层循环，进行array_length-1趟排序*/
	{
		min_value = array[i];		  /*第i趟排序将array[i]作为初始最小值*/
		min_index = i;

		for (j = i; j < array_length; j++)
		{
			if (array[j] < min_value)       /*寻找最小值*/
			{
				min_value = array[j];           /*将最小值保存到min_value*/
				min_index = j;
			}
		}

		if (min_index != i)
		{
			swap(&array[i], &array[min_index]);              /*将本次查询到的最小值与array[i]进行交换*/
		}
		/**************打印目前排序结果*********************/
		Print("\n After [%d] select sorting result:", i + 1);
		PrintArray(array, array_length, 0);
	}
}

#define ARRAY_MAX_SIZE (20)
#define SUPPER_SIZE (500)
static int s_array[ARRAY_MAX_SIZE + 1];
static int s_array_copy[ARRAY_MAX_SIZE + 1];

int main()
{
	int input_value;          /*读入输入值所使用的暂存变量*/
	int array_length = 0;
	int choice;
	int key = 0;
	int* supper_array_1, * supper_array_2, * supper_array_3;
	int i;
	clock_t start, end;
	double cpu_time_used;
	int search_data;

	printf("\n Please input the values you want to sort(Exit for 0):\n");
	printf("\n The number of data should be no more than 20.\n");

	/*************读取数值存入数组中*******************/
	scanf("%d", &input_value);   /*读输入值存到变量input_value*/
	while (input_value != 0                /*数列输入尚未结束*/
		&& array_length < ARRAY_MAX_SIZE)
	{
		s_array[++array_length] = input_value;
		scanf("%d", &input_value);
	}

	printf("\n The original data array is \n");
	PrintArray(s_array, array_length + 1, 1);

	while (1) {
		printf("\n------------options------------\n");
		printf("----------1.顺序查找-------------\n");
		printf("----------2.折半查找-------------\n");
		printf("----------3.冒泡排序-------------\n");
		printf("----------4.快速排序-------------\n");
		printf("----------5.性能统计-------------\n");
		printf("----------6.退    出-------------\n");
		printf("---------------------------------\n");
		printf("请选择（1,2,3,4,5,6）:");

		scanf("%d", &choice);

		memcpy(s_array_copy, s_array, sizeof(s_array));

		if (choice < 3) {
			printf("请输入查找元素：");
			scanf("%d", &key);
		}

		switch (choice)
		{
		case 1:
			EnSeqSearch(s_array_copy, key, array_length);
			//SeqSearch(&s_array_copy[1], key, array_length);
			break;
		case 2:
			/*折半查找前需要先排序*/
			BubbleSort(&s_array_copy[1], array_length);
			binarySearch(&s_array_copy[1], key, array_length);
			break;
		case 3:
			BubbleSort(&s_array_copy[1], array_length);
			break;
		case 4:
			QuickSort(&s_array_copy[1], 0, array_length - 1);
			break;
		case 5:
			supper_array_1 = (int*)malloc(sizeof(int) * SUPPER_SIZE * 3 + 100);
			if (!supper_array_1) {
				printf("\n not enough memory exit \r\n");
				break;
			}
			supper_array_2 = supper_array_1 + SUPPER_SIZE;
			supper_array_3 = supper_array_2 + SUPPER_SIZE;
			int seed = time(NULL); // 设置随机数种子为时间戳
			srand(seed);
			for (i = 0; i < SUPPER_SIZE; i++)
			{
				supper_array_1[i] = rand() % SUPPER_SIZE;
				supper_array_2[i] = supper_array_1[i];
				supper_array_3[i] = supper_array_1[i];
			}

			start = clock(); // 记录开始时钟时间
			QuickSort(supper_array_1, 0, SUPPER_SIZE - 1);
			end = clock();
			cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC; // 计算算法消耗时间
			printf("QuickSort used %f seconds.\n", cpu_time_used);

			start = clock(); // 记录开始时钟时间
			BubbleSort(supper_array_2, SUPPER_SIZE);
			end = clock();
			cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC; // 计算算法消耗时间
			printf("BubbleSort used %f seconds.\n", cpu_time_used);

			start = clock(); // 记录开始时钟时间
			for (i = 0; i < SUPPER_SIZE; i++)
				SeqSearch(supper_array_2, supper_array_3[i], SUPPER_SIZE);
			end = clock();
			cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC; // 计算算法消耗时间
			printf("SeqSearch used %f seconds.\n", cpu_time_used);

			start = clock(); // 记录开始时钟时间
			for (i = 0; i < SUPPER_SIZE; i++)
				binarySearch(supper_array_2, supper_array_3[i], SUPPER_SIZE);
			end = clock();
			cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC; // 计算算法消耗时间
			printf("binarySearch used %f seconds.\n", cpu_time_used);

			free(supper_array_1);
			break;
		case 6:
		default:
			return 0;
		}

		if (choice >= 3 && choice < 5) {
			/****************打印最终排序结果*****************/
			printf("\n Final sorting result:");
			PrintArray(&s_array_copy[1], array_length, 0);
		}
	}
	return 0;
}

#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)
#include "contact.h"

//通讯录
//1. 创建一个能存放100人的通讯录
//2. 姓名，年龄，性别，手机号，地址
//3. 添加联系人
//4. 删除联系人
//5. 查找联系人
//6. 修改联系人
//7. 显示联系人
//8. 排序
//9. 清空列表信息

void meun()
{
	printf("****************************************\n");
	printf("******    1. Add       2. Del    *******\n");
	printf("******    3. Seek      4. Modify *******\n");
	printf("******    5. Show      6. Sort   *******\n");
	printf("******    7. Empty     0. Exit   *******\n");
	printf("****************************************\n");
}
enum Option
{
	Exit,
	Add,
	Del,
	Seek,
	Modify,
	Show,
	Sort
};
int main()
{
	int input = 0;
	Contact con;
	InitContact(&con);
	do
	{
		meun();
		printf("请输入:>");
		scanf("%d", &input);
		switch (input)
		{
		case Add:
			AddContact(&con);
			break;
		case Del:
			DelContact(&con);
			break;
		case Seek:
			SeekContact(&con);
			break;
		case Modify:
			ModifyContact(&con);
			break;
		case Show:
			ShowContact(&con);
			break;
		case Sort:
			SortContact(&con);
			ShowContact(&con);
			break;
		case Exit:
			SoveContact(&con);
			DestoryContact(&con);
			break;
		default:
			printf("选择错误，请重新选择\n");
			break;

		}
	} while (input);
	return 0;
}
#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

#define MAX 100
#define MAX_NAME 20
#define MAX_SEX 4
#define MAX_TELE 20
#define MAX_ADDR 30

#define INIT_capacity 3
#define ADD_capacity 2

//表示一个人的信息
typedef struct PeoInfo
{
	char name[MAX_NAME];
	int age;
	char sex[MAX_SEX];
	char tele[MAX_TELE];
	char addr[MAX_ADDR];
}PeoInfo;

//typedef struct contact
//{
//	PeoInfo data[MAX];//存放数据
//	int sz;//记录通讯录中的有效信息的个数
//}Contact;

typedef struct contact
{
	PeoInfo* data;//存放数据
	int sz;//记录通讯录中的有效信息的个数
	int capacity; //记录容量
}Contact;


//初始化通讯录
void InitContact(Contact* pc);

//添加联系人信息
void AddContact(Contact* pc);

//显示全部联系人信息
void ShowContact(const Contact* pc);

//删除联系人信息
void DelContact(Contact* pc);

//查找联系人信息
void SeekContact(const Contact* pc);

//修改联系人信息
void ModifyContact(Contact* pc); 

//排序通讯录信息
void SortContact(Contact* pc);

//清空列表信息
void DestoryContact(Contact* pc);

//保存列表信息
void SoveContact(Contact* pc);
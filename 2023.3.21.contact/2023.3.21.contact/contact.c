#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include "contact.h"

//void InitContact(Contact* pc)
//{
//	assert(pc != NULL);
//
//	pc->sz = 0;
//	memset(pc->data, 0, sizeof(pc->data));
//}

static int CheckCapacity(Contact* pc);


static void LoadContact(Contact* pc)
{
	assert(pc != NULL);

	FILE* pf = fopen("test.txt", "rb");
	if (pf == NULL)
	{
		perror("LoadContact::fopen");
		return;
	}
	PeoInfo tmp = { 0 };
	while (fread(&tmp, sizeof(PeoInfo), 1, pf))
	{
		CheckCapacity(pc);
		pc->data[pc->sz] = tmp;
		pc->sz++;
	}

	fclose(pf);
	pf = NULL;

	printf("加载成功\n");
}

void InitContact(Contact* pc)
{
	assert(pc != NULL);

	pc->data = (PeoInfo*)malloc(INIT_capacity * sizeof(PeoInfo));
	if (pc->data == NULL)
	{
		perror("malloc");
		return;
	}
	pc->capacity = INIT_capacity;
	pc->sz = 0;
	
	//加载文件的信息到通讯录
	LoadContact(pc);
}

//非法返回-1，否则返回这个数
static int JudgeIsLegal(const Contact* pc)
{
	assert(pc != NULL);
	if (pc->sz >= MAX && pc->sz < 0)
		return -1;
	return pc->sz;
}

static int CheckCapacity(Contact* pc)
{
	assert(pc != NULL);

	if (pc->sz == pc->capacity)
	{
		PeoInfo* pf = (PeoInfo*)realloc(pc->data, (pc->capacity + ADD_capacity) * sizeof(PeoInfo));
		if (pf == NULL)
		{
			perror("realloc");
			return 0;
		}
		pc->data = pf;
		pc->capacity += ADD_capacity;

		printf("增容成功：%d\n", pc->capacity);
	}
	return 1;
}
void AddContact(Contact* pc)
{
	assert(pc != NULL);
	if (JudgeIsLegal(pc) == -1)
		return;
	if (0 == CheckCapacity(pc))
	{
		printf("空间不够， 增容失败\n");
		return;
	}

	printf("请输入联系人姓名:>");
	scanf("%s", pc->data[pc->sz].name);
	printf("请输入联系人年龄:>");
	scanf("%d", &(pc->data[pc->sz].age));
	printf("请输入联系人性别:>");
	scanf("%s", pc->data[pc->sz].sex);
	printf("请输入联系人电话:>");
	scanf("%s", pc->data[pc->sz].tele);
	printf("请输入联系人地址:>");
	scanf("%s", pc->data[pc->sz].addr);

	pc->sz++;
	printf("添加成功\n");
}

void ShowContact(const Contact* pc)
{
	assert(pc != NULL);
	printf("%-10s %-4s %-5s %-12s %-30s\n", 
		"姓名", "年龄", "性别", "手机号", "地址");
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		printf("%-10s %-4d %-5s %-12s %-30s\n",
			pc->data[i].name,
			pc->data[i].age,
			pc->data[i].sex,
			pc->data[i].tele,
			pc->data[i].addr);
	}
}
//找到了返回下标i，没找到返回0
static int FindMyCon(const Contact* pc, const char* name)
{
	assert(pc != NULL);
	assert(name != NULL);
	
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		if (0 == strcmp(pc->data[i].name, name))
			return i;
	}
	return -1;
}
void DelContact(Contact* pc)
{
	assert(pc != NULL);
	char name[100] = { 0 };
	if (JudgeIsLegal(pc) == 0 || JudgeIsLegal(pc) == -1)
		return;
	printf("请输入要删除人的姓名:>");
	scanf("%s", name);
	int pos = FindMyCon(pc, name);
	if (-1 == pos)
	{
		printf("不存在要删除的联系人\n");
		return;
	}
	int i = 0;
	for (i = pos; i < pc->sz - 1; i++)
	{
		pc->data[i] = pc->data[i + 1];
	}

	pc->sz--;
	printf("删除成功\n");
}

void SeekContact(const Contact* pc)
{
	assert(pc != NULL);
	char name[100] = { 0 };
	printf("请输入要查找人的姓名:>");
	scanf("%s", name);
	int pos = FindMyCon(pc, name);
	if (-1 == pos)
	{
		printf("不存在要查找的联系人\n");
		return;
	}

	printf("%-10s %-4s %-5s %-12s %-30s\n", 
		"姓名", "年龄", "性别", "手机号", "地址");
	printf("%-10s %-4d %-5s %-12s %-30s\n",
		pc->data[pos].name,
		pc->data[pos].age,
		pc->data[pos].sex,
		pc->data[pos].tele,
		pc->data[pos].addr);
}

void ModifyContact(Contact* pc)
{
	assert(pc != NULL);

	char name[100] = { 0 };
	printf("请输入要修改人的姓名:>");
	scanf("%s", name);
	int pos = FindMyCon(pc, name);
	if (-1 == pos)
	{
		printf("不存在要修改的联系人\n");
		return;
	}
	printf("请输入联系人姓名:>");
	scanf("%s", pc->data[pos].name);
	printf("请输入联系人年龄:>");
	scanf("%d", &(pc->data[pos].age));
	printf("请输入联系人性别:>");
	scanf("%s", pc->data[pos].sex);
	printf("请输入联系人电话:>");
	scanf("%s", pc->data[pos].tele);
	printf("请输入联系人地址:>");
	scanf("%s", pc->data[pos].addr);

	printf("修改成功\n");
}
int name_cmp(const void* e1, const void* e2)
{
	return strcmp(((PeoInfo*)e1)->name, ((PeoInfo*)e2)->name);
}
void SortContact(Contact* pc)
{
	assert(pc != NULL);
	qsort(pc, pc->sz, sizeof(PeoInfo), name_cmp);
}

void DestoryContact(Contact* pc)
{
	assert(pc != NULL);

	free(pc->data);
	pc->data = NULL;
	pc->capacity = 0;
	pc->sz = 0;

	printf("释放成功，退出程序\n");
}

void SoveContact(Contact* pc)
{
	assert(pc != NULL);

	FILE* pf = fopen("test.txt", "wb");
	if (pf == NULL)
	{
		perror("SoveContact::fopen");
		return;
	}
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		fwrite(pc->data + i, sizeof(PeoInfo), 1, pf);
	}

	fclose(pf);
	pf = NULL;

	printf("保存成功\n");
}
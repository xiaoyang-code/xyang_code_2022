#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)
#include <stdio.h>


////写一个函数返回参数二进制中 1 的个数。
//int count_int_bit1(unsigned int num)  
//{
//	// 使用unsigned int的原因是：这个函数对负数有bug存在
//	int count = 0;
//	while (num)
//	{
//		if (num % 2 == 0)
//		{
//			count++;
//		}
//		num /= 2;
//	}
//	return count;
//}
//int count_int_bit2(int num)
//{
//	int count = 0;
//	for (int i = 0; i < 32; i++)
//	{
//		if (((num >> i) & 1) == 1)
//		{
//			count++;
//		}
//	}
//	return count;
//}
//int count_int_bit3(int num)
//{
//	int count = 0;
//	while (num)
//	{
//		num = num & (num - 1);
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	//int ret = count_int_bit1(num);
//	//int ret = count_int_bit2(num);
//	int ret = count_int_bit3(num);
//	printf("%d\n", ret);
//	return 0;
//}


////两个int（32位）整数m和n的二进制表达中，有多少个位(bit)不同？
//int main()
//{
//	int n = 0;
//	int m = 0;
//	int count = 0;
//	scanf("%d %d", &n, &m);
//
//	/*for (int i = 0; i < 32; i++)
//	{
//		if (((n >> i) & 1) == ((m >> i) & 1))
//		{
//			count++;
//		}
//	}*/
//
//	// 异或法 
//	int ret = n ^ m;
//	while (ret)
//	{
//		ret = ret & (ret - 1);
//		count++;
//	}
//	printf("%d ", count);
//	return 0;
//}


////如何判断一个数是不是2的幂次方
//int fun(int num)
//{
//	int flag = 0;
//	while (num)
//	{
//		num = num & (num - 1);
//		flag++;
//	}
//	if (flag == 1)
//	{
//		return 1;
//	}
//	return 0;
//}
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	if (fun(num) == 1)
//	{
//		printf("yes\n");
//	}
//	else
//	{
//		printf("no\n");
//	}
//	return 0;
//}

//int main()
//{
//    int a = 0x11223344;
//    char* pc = (char*)&a;
//    *pc = 0;
//    printf("%x\n", a);
//    return 0;
//}

//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* parr = &arr;
//	for (int i = 0; i < sizeof(arr) / sizeof(*arr); i++)
//	{
//		printf("%d ", *(parr + i));
//	}
//
//	return 0;
//}

#include <math.h>
//
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 100000; i++)
//	{
//		int sum = 0;
//		int count = 0;
//		int num1 = i;
//		while (num1)
//		{
//			num1 /= 10;
//			count++;
//		}
//		int num2 = i;
//		while (num2)
//		{
//			sum += pow(num2 % 10, count);
//			num2 /= 10;
//		}
//		if (i == sum)
//		{
//			printf("%d ", sum);
//		}
//	}
//	return 0;
//}


//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int sum = n;
//	int ret = n;
//	for (int i = 0; i < 5 - 1; i++)
//	{
//		n *= 10;
//		sum = sum + n;
//		ret += sum;
//	}
//	printf("%d\n", ret);
//	return 0;
//}

#include <string.h>
//
//int main()
//{
//    char arr[] = "i am a handsome.";
//    int l = 0;
//    int r = strlen(arr) - 1;
//    while (l < r)
//    {
//        char tmp = arr[l];
//        arr[l] = arr[r];
//        arr[r] = tmp;
//        l++;
//        r--;
//    }
//    printf("%s", arr);
//    return 0;
//}


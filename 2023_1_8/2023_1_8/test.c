#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>

//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	switch (a)
//	{
//	case 1:printf("1\n"); break;
//	case 2:printf("2\n"); break;
//	default:printf("789\n"); break;
//	case 3:printf("3\n"); break;
//	case 4:printf("4\n"); break;
//	}
//	return 0;
//}


//int main()
//{
//	int i = 1;
//	while (i <= 100)
//	{
//		if (i % 3 == 0)
//		{
//			printf("%d ", i);
//		}
//		i++;
//	}
//	return 0;
//}


//int main()
//{
//	int arr[3] = { 0 };
//	for (int i = 0; i < 3; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	for (int i = 0; i < 3 - 1; i++)
//	{
//		for (int j = 0; j < 3 - i - 1; j++)
//		{
//			if (arr[j] < arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//	for (int i = 0; i < 3; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}


//int main()
//{
//	for (int i = 100; i <= 200; i++)
//	{
//		int flag = 1;
//		for (int j = 2; j <= sqrt(i); j++)
//		{
//			if (i % j == 0)
//			{
//				flag = 0;
//			}
//		}
//		if (1 == flag)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}


//int main()
//{
//	int i = 0;
//	for (i = 1000; i <= 2000; i++)
//	{
//		if (i % 4 == 0 && i % 100 != 0 || i % 400 == 0)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int fun(int a, int b)
//{
//	int c = 1;
//	while (c)
//	{
//		c = a % b;
//		a = b;
//		b = c;
//	}
//	return a;
//}
//int main()
//{
//	int a = 0, b = 0;
//	scanf("%d %d", &a, &b);
//	printf("%d\n", fun(a, b));
//	return 0;
//}


//牛客网新手训练122 —— 有序序列判断
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int arr[101] = { 0 };
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    int pos = 0, neg = 0, equ = 0;  //  重点 —— 通过计数来判断
//    for (int i = 0; i < n - 1; i++)
//    {
//        if (arr[i] > arr[i + 1])
//        {
//            pos++;
//        }
//        else if (arr[i] < arr[i + 1])
//        {
//            neg++;
//        }
//        else
//        {
//            equ++;
//        }
//    }
//    if (n - 1 == pos || n - 1 == neg || n - 1 == equ)  // 易错点  （n-1）
//    {
//        printf("sorted\n");
//    }
//    else
//    {
//        printf("unsorted\n");
//    }
//    return 0;
//}

//牛客网新手训练123 —— 有序序列插入一个整数
//int main()
//{
//    int n = 0;
//    int arr[51] = { 0 };
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    int a = 0;
//    scanf("%d", &a);  // 此处可写一个循环  ——  插入n个数
//    arr[n] = a;
//    for (int i = 0; i < n; i++)  //  经典 —— 冒泡排序
//    {
//        for (int j = 0; j < n - i; j++)
//        {
//            if (arr[j] > arr[j + 1])
//            {
//                int tmp = arr[j];
//                arr[j] = arr[j + 1];
//                arr[j + 1] = tmp;
//            }
//        }
//    }
//    for (int i = 0; i <= n; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//}



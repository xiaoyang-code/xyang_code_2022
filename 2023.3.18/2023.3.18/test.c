#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>
#include <string.h>
#include <assert.h>

//void reverse(char* arr)
//{
//	assert(arr != NULL);
//	char* left = arr;
//	char* right = arr + strlen(arr) - 1;
//	while (left < right)
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	char arr[101];
//	while (gets(arr))
//	{
//		reverse(arr);
//		printf("%s\n", arr);
//		memset(arr, 0, sizeof(arr) / sizeof(arr[0]));
//	}
//	return 0;
//}


void test1()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9 };
	int len = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = len - 1;

	while (left < right)
	{
		//找偶数
		while (arr[left] % 2 != 0 && left < len)
		{
			left++;
		}
		//找奇数
		while (arr[right] % 2 != 1 && left < right)
		{
			right--;
		}
		//交换
		if (left < right)
		{
			int tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
		}
	}
	//打印
	int i = 0;
	for (i = 0; i < len; i++)
	{
		printf("%d ", arr[i]);
	}
}
void test2_1()
{
	/*5位运动员参加了10米台跳水比赛，有人让他们预测比赛结果：
	A选手说：B第二，我第三；
	B选手说：我第二，E第四；
	C选手说：我第一，D第二；
	D选手说：C最后，我第三；
	E选手说：我第四，A第一；
	比赛结束后，每位选手都说对了一半，请编程确定比赛的名次。*/
	int a, b, c, d, e;
	for (a = 1; a <= 5; a++)
	{
		for (b = 1; b <= 5; b++)
		{
			for (c = 1; c <= 5; c++)
			{
				for (d = 1; d <= 5; d++)
				{
					for (e = 1; e <= 5; e++)
					{
						if (
							(b == 2) + (a == 3) == 1 &&
							(b == 2) + (e == 4) == 1 &&
							(c == 1) + (d == 2) == 1 &&
							(c == 5) + (d == 3) == 1 &&
							(e == 4) + (a == 1) == 1 &&
							a*b*c*d*e == 120
							)
						{
							printf("%d %d %d %d %d\n", a, b, c, d, e);
						}
					}
				}
			}
		}
	}
}
int checkData1(int* p)
{
	int tmp[6] = { 0 }; //标记表

	int i = 0;
	for (i = 0; i < 5; i++)
	{
		if (tmp[p[i]]) // 如果表达式为真，则说明此处已经被占用了
		{
			return 0;
		}
		tmp[p[i]] = 1;
	}
	return 1;
}
void test2_2()
{
	int p[5] = { 0 };
	for (p[0] = 1; p[0] <= 5; p[0]++)
	{
		for (p[1] = 1; p[1] <= 5; p[1]++)
		{
			for (p[2] = 1; p[2] <= 5; p[2]++)
			{
				for (p[3] = 1; p[3] <= 5; p[3]++)
				{
					for (p[4] = 1; p[4] <= 5; p[4]++) 
					{
							if ((p[1] == 2) + (p[0] == 3) == 1 && 
							(p[1] == 2) + (p[4] == 4) == 1 && 
							(p[2] == 1) + (p[3] == 2) == 1 && 
							(p[2] == 5) + (p[3] == 3) == 1 && 
							(p[4] == 4) + (p[0] == 1) == 1 && 
							checkData1(p) //不能并列
							)
						{
							for (int i = 0; i < 5; i++)
							{
								printf("%d ", p[i]);
							}
							putchar('\n');
						}
					}
				}
			}
		}
	}
}
int checkData2(int* p)
{
	char tmp = 0;

	int i;
	for (i = 0; i < 5; i++)
	{
		tmp |= 1 << p[i];
		//tmp每次或上一位1，p[i]如果是1~5都有，则1<<1到1<<5都或上的结果将会是00111110，
		//如果有并列，则一定会至少却其中一个1，结果就不会是00111110，
		//所以可以判断tmp最终的结果是不是这个数字来判断有没有重复。
	}
	return tmp == 0x3E;
}
void diveRank(int* p, int n)
{
	if (n >= 5)
	{
		if ((p[1] == 2) + (p[0] == 3) == 1 && 
			(p[1] == 2) + (p[4] == 4) == 1 && 
			(p[2] == 1) + (p[3] == 2) == 1 && 
			(p[2] == 5) + (p[3] == 3) == 1 && 
			(p[4] == 4) + (p[0] == 1) == 1 && 
			checkData2(p)) //查重
		{
			for (int i = 0; i < 5; i++)
			{
				printf("%d ", p[i]);
			}
			putchar('\n');
		}
		return;
	}
	for (p[n] = 1; p[n] <= 5; p[n]++)  //通过递归模拟多层循环，每进一次递归相当于进了一层新的循环。
	{
		diveRank(p, n + 1);
	}
}
void test2_3()
{
	int p[5] = { 0 };
	diveRank(p, 0);
}
void test3()
{
	/*日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。
	以下为4个嫌疑犯的供词:
	A说：不是我。
	B说：是C。
	c说：是D。
	D说：C在胡说
	已知3个人说了真话，1个人说的是假话。
	现在请根据这些信息，写一个程序来确定到底谁是凶手。*/
	char killer = 0;
	for (char killer = 'a'; killer <= 'd'; killer++)
	{
		if ((killer != 'a') + (killer == 'c') + (killer == 'd') + (killer != 'd') == 3)
		{
			printf("%c\n", killer);
		}
	}
	
}
void reverse1(char* str, int k)
{
	assert(str != NULL);

	int len = (int)strlen(str);
	k %= len;  //防止无效循环
	while (k--)
	{
		char tmp = *str;
		for (int i = 1; i < len; i++)
		{
			*(str + i - 1) = *(str + i);
		}
		*(str + len - 1) = tmp;
	}
}
void test4()
{
	char arr[] = "abcdef";
	int k = 0;
	scanf("%d", &k);
	reverse1(arr, k);
	printf("%s\n", arr);
}
void test4_2()
{
	char arr[] = "abcdef";
	int len = (int)strlen(arr);
	int k = 0;
	scanf("%d", &k);
	k %= len;
	char tmp[10001] = "0";
	strcpy(tmp, arr + k);
	strncat(tmp, arr, k);
	strcpy(arr, tmp);
	printf("%s\n", arr);
}
void reverse2(char* str, int start, int end)
{
	assert(str != NULL); 

	while (start < end)
	{
		char tmp = *(str + start);
		*(str + start) = *(str + end);
		*(str + end) = tmp;
		start++;
		end--;
	}
}
void test4_3()
{
	char arr[] = "abcdefg";
	int len = (int)strlen(arr);
	int k = 0;
	scanf("%d", &k);
	k %= len;
	reverse2(arr, 0, k - 1); //左半边逆序
	reverse2(arr, k, len - 1);  //右半边逆序
	reverse2(arr, 0, len - 1);  //整体逆序
	printf("%s\n", arr);
}
int find_num_1(int arr[3][3], int row, int col, int k)
{
	int x = 0;
	int y = col - 1;
	while (y>=0 && x <= row-1)
	{
		if (arr[x][y] > k)
		{
			y--;
		}
		else if (arr[x][y] < k)
		{
			x++;
		}
		else
		{
			return 1;
		}
	}
	return 0;
}
void test5_1()
{
	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
	int k = 0;
	scanf("%d", &k);
	int ret = find_num_1(arr, 3, 3, k);
	if (ret == 1)
	{
		printf("找到啦\n");
	}
	else
	{
		printf("没找到\n");
	}
}
int find_num_2(int arr[3][3], int* px, int* py, int k)
{
	int x = 0;
	int y = *py - 1;
	while (y >= 0 && x <= *px - 1)
	{
		if (arr[x][y] > k)
		{
			y--;
		}
		else if (arr[x][y] < k)
		{
			x++;
		}
		else
		{
			*px = x;
			*py = y;
			return 1;
		}
	}
	return 0;
}
void test5_2()
{
	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
	int k = 0;
	scanf("%d", &k);
	int x = 3;
	int y = 3;
	//返回型参数
	int ret = find_num_2(arr, &x, &y, k);
	if (ret == 1)
	{
		printf("找到啦,下标是：%d %d\n",x,y);
	}
	else
	{
		printf("没找到\n");
	}
}
int find_str(const char* str, char* find)
{
	char tmp[202] = "0";
	size_t len = strlen(str);
	strcpy(tmp, str);
	strncat(tmp, str, len-1);
	return (strstr(tmp, find) != NULL);
}
void test6()
{
	char arr1[] = "abcde";
	char arr2[101] = "0";
	scanf("%s", &arr2);
	int ret = find_str(arr1, arr2);
	printf("%d", ret);
}
void test7()
{
	struct   //匿名结构体类型 —— 只能定义一次
	{
		int a;
		char c;
	}s;

	s.a = 1;
	s.c = 'a';
	printf("%d\n%c", s.a,s.c);
}
int main()
{
	//test1();
	//test2_1();
	//test2_2();
	//test2_3();
	//test3();
	//test4_1();
	//test4_2();
	//test4_3();
	//test5_1();
	//test5_2();
	//test6();
	test7();
	return 0;
}
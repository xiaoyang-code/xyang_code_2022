#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
////qsort函数的使用
//int int_cmp(const void* p1, const void* p2)
//{
//	return (*(int*)p1 - *(int*)p2);
//}
//struct stu
//{
//	int age;
//	char name[20];
//};
//int cmp_stu_age(const void* p1, const void* p2)
//{
//	return (((struct stu*)p1)->age - ((struct stu*)p2)->age);
//}
//int cmp_stu_name(const void* p1, const void* p2)
//{
//	return strcmp(((struct stu*)p1)->name, ((struct stu*)p2)->name);
//}
//int main()
//{
//	int arr[10] = { 10,9,8,7,6,5,4,3,2,1 };
//	int sz = sizeof(arr) / sizeof(*arr);
//	qsort(arr, sz, sizeof(int), int_cmp);
//	struct stu s[] = { {20,"xiaoming"},{30,"zhangshuai"},{25,"laoli"} };
//	int sz2 = sizeof(s) / sizeof(s[0]);
//	//qsort(s, sz2, sizeof(struct stu), cmp_stu_age);
//	//qsort(s, sz2, sizeof(struct stu), cmp_stu_name);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

////qsort函数的模拟实现（使用冒泡函数）

void swap(char* buf1, char* buf2, size_t width)
{
	char tmp = *buf1;
	*buf1 = *buf2;
	*buf2 = tmp;
}
int int_cmp(const void* p1, const void* p2)
{
	return (*(int*)p1 - *(int*)p2);
}
void bubble_qsort(void* base, size_t num, size_t width, int (*cmp)(const void* p1, const void* p2))
{
	for (int i = 0; i < num; i++)
	{
		for (int j = 0; j < num; j++)
		{
			if (cmp( (char*)base + j * width , (char*)base + (j+1) * width) > 0)
			{
				swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
			}
		}
	}
}
int main()
{
	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(*arr);
	bubble_qsort(arr, sz, sizeof(int), int_cmp);

	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////

	//数组和指针
	//数组 - 能够存放一组相同类型的元素，数组的大小取决于数组的元素个数和元素类型
	//指针 - 地址/指针变量，大小是4/8个字节
	//数组是数组，指针是指针，二者不等价
	//数组名是数组首元素的地址，这个地址可以存放在指针变量中
	//我们就可以使用指针来遍历数组
	//
	//数组名
	//大部分情况下，数组名是数组首元素的地址
	//但有2个例外
	//sizeof（数组名） - 数组名表示整个数组，计算的是整个数组的大小
	//&数组名 - 数组名表示整个数组，取出的是数组的地址
//
//int main()
//{
//	int a[] = { 1,2,3,4 };
//	printf("%d\n", sizeof(a));  //  16
//	printf("%d\n", sizeof(a + 0));  //  4
//	printf("%d\n", sizeof(*a));  //  4
//	printf("%d\n", sizeof(a + 1));  //  4
//	printf("%d\n", sizeof(a[1]));  //  4
//	printf("%d\n", sizeof(&a));  //  4/8
//	printf("%d\n", sizeof(*&a));  //  16
//	printf("%d\n", sizeof(&a + 1));  //  16
//	printf("%d\n", sizeof(&a[0]));  //  4/8
//	printf("%d\n", sizeof(&a[0] + 1));  //  4/8
//
//	return 0;
//}
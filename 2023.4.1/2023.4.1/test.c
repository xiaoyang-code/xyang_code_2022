#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>
#include <math.h>
#include <string.h>

void test1()
{
	int n = 0;
	int yuan = 0;
	int fen = 0;
	scanf("%d", &n);
	for (yuan = 0; yuan < 100; yuan++)
	{
		for (fen = 0; fen < 100; fen++)
		{
			if ((200 * yuan + 2 * fen) == (100 * fen + yuan - n))
			{
				printf("%d.%d", yuan, fen);
				return 0;
			}
		}
	}
	printf("No Solution\n");
	return 0;
}
void test2()
{
	int n = 0;
	int i = 0;
	int j = 0;
	scanf("%d", &n);
	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= i; j++)
		{
			printf("%d*%d=%-4d", j, i, i * j);
		}
		printf("\n");
	}
	return 0;
}
void test3()
{
	int n = 0;
	int x = 0;
	int y = 0;
	int flag = 0;
	scanf("%d", &n);
	for (x = 1; x <= sqrt(n); x++)
	{
		for (y = 1; y <= sqrt(n); y++)
		{
			if ((pow(x, 2) + pow(y, 2) == n) && (x<=y))
			{
				printf("%d %d\n", x, y);
				flag = 1;
			}
		}
	}
	if (flag == 0)
	{
		printf("No Solution\n");
	}
	return 0;
}
void test4()
{
	int t = 0;
	int i = 0;
	int turtle = 0;
	int rabbit = 0;
	scanf("%d", &t);
	for (i = 1; i <= t; i++)
	{
		if ((i % 10 == 0) && (rabbit > turtle))
		{
			for (int j = 0; j < 30, i <= t; j++, i++)
			{
				turtle += 3;
			}
		}
		else
		{
			turtle += 3;
			rabbit += 9;
		}
	}
	if (rabbit == turtle)
	{
		printf("-_- %d\n",turtle);
	}
	else if (rabbit > turtle)
	{
		printf("^_^ %d\n", rabbit);
	}
	else
	{
		printf("@_@ %d\n", turtle);
	}
	return 0;
}
float fun(float* arr, float x)
{
	float ret = 0.0f;
	ret = (*(arr + 0)) * pow(x, 3) + (*(arr + 1)) * pow(x, 2) + (*(arr + 2)) * x + (*(arr + 3));
	return ret;
}
void test5()
{
	float arr[4] = { 0.0f };
	float a = 0.0f;
	float b = 0.0f;
	
	int i = 0;
	for (i = 0; i < 4; i++)
	{
		scanf("%f", &arr[i]);
	}
	scanf("%f %f", &a, &b);
	
	while(fun(arr, a) * fun(arr, b) < 0)
	{
		if (fun(arr, (a + b) / 2.0) == 0.0)
		{
			printf("%.2f\n", (a + b) / 2);
			return 0;
		}
		else if (fun(arr, (a + b) / 2) * fun(arr, a) > 0)//ͬ��
		{
			a = (a + b) / 2;
		}
		else
		{
			b = (a + b) / 2;
		}
	}
}
char* str_change(char* str)
{
	int count = 0;
	int num = 0;
	while (*str)
	{
		*str += 49;
		str++;
		num++;
	}
	char arr[20] = {0};
	int i = 0;
	while (num--)
	{
		count++;
		str--;
		if (*str != 0 && count == 2)
		{
			arr[i] = 'S';
			i++;
		}
		else if (*str != 0 && count == 3)
		{
			arr[i] = 'B';
			i++;
		}
		else if (*str != 0 && count == 4)
		{
			arr[i] = 'Q';
			i++;
		}
		else if (*str != 0 && count == 5)
		{
			arr[i] = 'W';
			i++;
		}
		else if (*str != 0 && count == 9)
		{
			arr[i] = 'Y';
			i++;
		}
		arr[i] = *str;
		i++;
	}

	strcpy(str,arr);
	return str;
}
void test6()
{
	char arr[20] = "0";
	scanf("%s", arr);
	str_change(arr);
	printf("%s\n", arr);
	return 0;
}
int main()
{
	//test1();
	//test2();
	//test3();	
	//test4();
	//test5();
	test6();
	return 0;
}
#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>


//个人信息宏定义
#define MAX_Name 20
#define MAX_Sex  4
#define MAX_Tele 12
#define MAX_Addr 30

//通讯录信息宏定义
#define INIT_Capacity 3
#define ADD_Capacity 2

//一个人的信息
typedef struct PeoInfo
{
	char Name[MAX_Name];
	int Age;
	char Sex[MAX_Sex];
	char Tele[MAX_Tele];
	char Addr[MAX_Addr];
}PeoInfo;

//动态通讯录
typedef struct Contest
{
	PeoInfo* data;
	int sz;
	int capacity;

}Contest;

//初始化通讯录
void InitContest(Contest* pc);

//添加联系人
void AddContest(Contest* pc);

//删除联系人
void DelContest(Contest* pc);

//查找联系人
void SeekContest(Contest* pc);

//修改联系人
void ReviseContest(Contest* pc);

//显示联系人
void ShowContest(Contest* pc);

//排序联系人
void SortContest(Contest* pc);

//保存联系人
void SoveContest(Contest* pc);

//销毁联系人
void DestroyContest(Contest* pc);
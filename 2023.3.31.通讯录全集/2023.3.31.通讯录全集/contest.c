#define _CRT_SECURE_NO_WARNINGS 1
#pragma warning(disable:6031)

#include "contest.h"


static void CheckContest(Contest* pc)
{
	assert(pc != NULL);

	if (pc->capacity == pc->sz)
	{
		PeoInfo* str = (PeoInfo*)realloc(pc->data, (pc->capacity + ADD_Capacity) * sizeof(PeoInfo));
		if (str == NULL)
		{
			perror("realloc");
			return ;
		}
		pc->data = str;
		pc->capacity += ADD_Capacity;

		printf("增容成功，当前容量：%d\n", pc->capacity);
	}
}


static void LoadContest(Contest* pc)
{
	assert(pc != NULL);

	FILE* pf = fopen("contest.txt", "rb");
	if (pf == NULL)
	{
		perror("LoadContest::fopen");
		return;
	}

	PeoInfo tmp = { 0 };
	while (fread(&tmp, sizeof(PeoInfo), 1, pf))
	{
		CheckContest(pc);
		pc->data[pc->sz] = tmp;
		pc->sz++;
	}

	printf("信息加载成功\n");
}

void InitContest(Contest* pc)
{
	assert(pc != NULL);

	pc->data = (PeoInfo*)malloc(INIT_Capacity * sizeof(PeoInfo));
	if (pc->data == NULL)
	{
		perror("InitContest::malloc");
		return;
	}
	pc->sz = 0;
	pc->capacity = INIT_Capacity;

	//将文件信息加载到通讯录中
	LoadContest(pc);
}

void AddContest(Contest* pc)
{
	assert(pc != NULL);

	CheckContest(pc);

	printf("请输入联系人姓名:>");
	scanf("%s", pc->data[pc->sz].Name);
	printf("请输入联系人年龄:>");
	scanf("%d", &(pc->data[pc->sz].Age));
	printf("请输入联系人性别:>");
	scanf("%s", pc->data[pc->sz].Sex);
	printf("请输入联系人电话:>");
	scanf("%s", pc->data[pc->sz].Tele);
	printf("请输入联系人地址:>");
	scanf("%s", pc->data[pc->sz].Addr);

	pc->sz++;
	printf("添加成功\n");
}

static int FindContest(const Contest* pc, const char* name)
{
	assert(pc != NULL);
	assert(name != NULL);

	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		if (0 == strcmp(name, pc->data[i].Name))
			return i;
	}
	return -1;
}
void DelContest(Contest* pc)
{
	assert(pc != NULL);

	printf("请输入想要删除人的姓名:>");
	char name[MAX_Name] = { 0 };
	scanf("%s", name);

	int pos = 0;
	pos = FindContest(pc, name);

	if (pos == -1)
	{
		printf("找不到要删除的联系人\n");
		return;
	}
	int i = 0;
	for (i = pos; i < pc->sz - 1; i++)
	{
		pc->data[i] = pc->data[i + 1];
	}
	
	pc->sz--;
	printf("删除成功\n");
}

void SeekContest(Contest* pc)
{
	assert(pc != NULL);

	printf("请输入想要查找人的姓名:>");
	char name[MAX_Name] = { 0 };
	scanf("%s", name);

	int pos = 0;
	pos = FindContest(pc, name);

	if (pos == 0)
	{
		printf("找不到要查找的联系人\n");
		return;
	}

	printf("%-20s %-5s %-4s %-12s %-30s\n",
		"姓名", "年龄", "性别", "电话", "地址");
	printf("%-20s %-5d %-4s %-12s %-30s\n",
		pc->data[pos].Name,
		pc->data[pos].Age,
		pc->data[pos].Sex,
		pc->data[pos].Tele,
		pc->data[pos].Addr);
}

void ReviseContest(Contest* pc)
{
	assert(pc != NULL);

	printf("请输入想要修改人的姓名:>");
	char name[MAX_Name] = { 0 };
	scanf("%s", name);

	int pos = 0;
	pos = FindContest(pc, name);

	if (pos == 0)
	{
		printf("找不到要修改的联系人\n");
		return;
	}

	printf("请输入联系人姓名:>");
	scanf("%s", pc->data[pos].Name);
	printf("请输入联系人年龄:>");
	scanf("%d", &(pc->data[pos].Age));
	printf("请输入联系人性别:>");
	scanf("%s", pc->data[pos].Sex);
	printf("请输入联系人电话:>");
	scanf("%s", pc->data[pos].Tele);
	printf("请输入联系人地址:>");
	scanf("%s", pc->data[pos].Addr);

	printf("修改成功\n");
}

void ShowContest(Contest* pc)
{
	assert(pc != NULL);

	printf("%-20s %-5s %-4s %-12s %-30s\n",
		"姓名", "年龄", "性别", "电话", "地址");
	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		printf("%-20s %-5d %-4s %-12s %-30s\n",
			pc->data[i].Name,
			pc->data[i].Age,
			pc->data[i].Sex,
			pc->data[i].Tele,
			pc->data[i].Addr);
	}
}

static int name_cmp(const void* e1, const void* e2)
{
	return strcmp(((PeoInfo*)e1)->Name, ((PeoInfo*)e2)->Name);
}
void SortContest(Contest* pc)
{
	assert(pc != NULL);

	qsort(pc->data, pc->sz, sizeof(PeoInfo), name_cmp);

	printf("排序成功\n");
}

void SoveContest(Contest* pc)
{
	assert(pc != NULL);

	FILE* pf = fopen("contest.txt", "wb");
	if (pf == NULL)
	{
		perror("SoveContest::fopen");
		return;
	}

	int i = 0;
	for (i = 0; i < pc->sz; i++)
	{
		fwrite(pc->data + i, sizeof(PeoInfo), 1, pf);
	}

	printf("信息保存成功\n");
}

void DestroyContest(Contest* pc)
{
	assert(pc != NULL);

	free(pc->data);
	pc->data = NULL;
	pc->capacity = 0;
	pc->sz = 0;

	printf("安全退出\n");
}